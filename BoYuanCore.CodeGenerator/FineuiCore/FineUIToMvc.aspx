﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FineUIToMvc.aspx.cs" Inherits="BoYuanCore.CodeGenerator.FineuiCore.FineUIToMvc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>FineUIPro Webform标签转Mvc view</title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager runat="server" AutoSizePanelID="form_Edit" />

        <f:Form ID="form_Edit" ShowBorder="True" ShowHeader="False" Title=" " runat="server" AutoScroll="true"
            EnableCollapse="false" BodyPadding="15px 15px">
            <Rows>
                <f:FormRow runat="server">
                    <Items>
                        <f:TextArea runat="server" ID="txa_old" Height="380px" Required="True" ShowRedStar="True" Label="FineUIPro WebForm 标签" />
                    </Items>
                </f:FormRow>


                <f:FormRow runat="server">
                    <Items>
                        <f:TextArea runat="server" ID="txa_new" Height="380px" Readonly="True" Label="Mvc view" />
                    </Items>
                </f:FormRow>

                <f:FormRow runat="server">
                    <Items>
                        <f:Grid ID="Grid1" runat="server" EnableHeaderMenu="False" Title="Grid" AllowPaging="False" EnableTextSelection="True" IsDatabasePaging="False" ShowHeader="False" Height="300px">

                            <Columns>
                                <f:BoundField runat="server" DataField="RowId" HeaderText="行号" Width="80px" TextAlign="Right"/>
                                <f:BoundField runat="server" DataField="Text" HeaderText="文本" ExpandUnusedSpace="True" />
                                <f:BoundField runat="server" DataField="Tag" HeaderText="行标签" Width="120px" />
                                <f:BoundField runat="server" DataField="TagType" HeaderText="行标签类型(配对rowid)" Width="160px" TextAlign="Right"/>
                                <f:BoundField runat="server" DataField="Level" HeaderText="层级" Width="80px" TextAlign="Right"/>
                                <f:BoundField runat="server" DataField="ParentId" HeaderText="父级" Width="80px" TextAlign="Right"/>
                                <f:CheckBoxField DataField="IsTraversal" HeaderText="是否删除" RenderAsStaticField="true" Width="80px" />

                            </Columns>
                        </f:Grid>
                    </Items>
                </f:FormRow>
            </Rows>

            <Toolbars>
                <f:Toolbar runat="server" Position="Top">
                    <Items>

                        <f:Button runat="server" ValidateForms="form_Edit" ID="btn_showCode" Text="转换代码" Icon="Html" OnClick="btn_showCode_OnClick" />
                        <f:ToolbarSeparator runat="server" />

                        <%--<f:Button runat="server" ValidateForms="form_Edit" ID="btn_delPar" Text="删除注释" Icon="Html" OnClick="btn_delPar_OnClick" />
                        <f:ToolbarSeparator runat="server" />--%>
                        <f:ToolbarText runat="server" Text="FineUIPro Webform标签转Mvc view。暂不支持Grid Column 转换;不支持特性值包含	&quot; &lt; &gt;" />
                    </Items>
                </f:Toolbar>
            </Toolbars>
        </f:Form>
    </form>
</body>
</html>