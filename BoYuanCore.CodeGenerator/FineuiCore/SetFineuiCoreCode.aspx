﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetFineuiCoreCode.aspx.cs" Inherits="BoYuanCore.CodeGenerator.FineuiCore.SetFineuiProCode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>FineUIPro代码生成工具</title>

    <link href="/Scripts/styles/vs.css" rel="stylesheet" />
</head>
<body>
    <script src="/Scripts/highlight.pack.js"></script>
    <script>hljs.configure({ useBR: true });/*使用code的自动换行，highlight.js不自行增加换行*/</script>
    <form id="form1" runat="server">
        <f:PageManager runat="server" AutoSizePanelID="Panel1" FormLabelAlign="Right" />
        <f:Panel ID="Panel1"  Margin="5px" runat="server" ShowBorder="false" ShowHeader="false" Layout="Region">
            <Items>

                <f:SimpleForm ID="SimpleForm1" ShowBorder="True" Layout="VBox" AutoScroll="true" ShowHeader="True" Title="(2) 自动生成FineuiCore代码。" BoxConfigChildMargin="0 0 5 0" BodyPadding="5" runat="server">
                    <Toolbars>
                        <f:Toolbar runat="server">
                            <Items>
                                <f:TextBox runat="server" Label="项目名称" ID="txb_NameSpace" ShowRedStar="True" Text="BoYuanCore" LabelWidth="100" Width="200px" />
                                <f:TextBox runat="server" Label="Areas名称" ID="txb_Areas" Text="Admin" LabelWidth="100" Width="170px" EmptyText="例: Admin" ShowRedStar="True" LabelAlign="Right" />
               
                                <f:TextBox runat="server" Label="继承Controller类名" ID="txb_BaseClassName" Text="BaseController" ShowRedStar="True" LabelWidth="160" Width="280px" LabelAlign="Right" />
                                <f:TextBox runat="server" ID="txb_savePath" Label="代码存放位置" Text="C:\Code" Required="True" ShowRedStar="True" LabelWidth="130px" Width="230px" />

                            </Items>
                        </f:Toolbar>
                        <f:Toolbar runat="server">
                            <Items>
                                <f:CheckBox runat="server" Label="生成页面" ID="cb_isHaveAddPage" LabelWidth="80" Text="编辑页" Checked="True" />
                                <f:CheckBox runat="server" ID="cb_isHaveGridPage" Text="列表页" Checked="True" />
                                <f:CheckBox runat="server" ID="cb_isHaveViewPage" Text="视图页" Hidden="True"/>
                                <f:CheckBox runat="server" ID="cb_isHaveEGPage" Text="可编辑列表页" Hidden="True"/>
                                <f:CheckBox runat="server" ID="cb_isHaveOnePage" Text="单页式" Hidden="True"/>
                                <f:Button runat="server" ID="Btn_SetSimpleCode" Text="生成简单代码" Icon="PageWhiteCsharp" OnClick="Btn_SetSimpleCode_OnClick" OnClientClick="if(!validate())return ;" ValidateForms="SimpleForm1" />

                            </Items>
                        </f:Toolbar>
                      
                        <f:Toolbar runat="server">
                            <Items>
                                <f:TextBox runat="server" ID="f_moudelUrl" Label="项目页面路径" Width="500px" LabelWidth="118px" />
                                <f:Button runat="server" ID="Btn_AddPageInfoToDB" Text="同步View到数据库" ConfirmText="确认要同步到数据库中？" Icon="DatabaseAdd" OnClick="Btn_AddPageInfoToDB_OnClick" OnClientClick="if(!validate_path())return;" />
                                <f:ToolbarSeparator runat="server" />
                                <f:Button runat="server" ID="Btn_AddButtonInfoToDB" Text="同步Action事件到数据库" ConfirmText="确认要同步到数据库中？" Icon="Button" OnClick="Btn_AddButtonInfoToDB_OnClick" OnClientClick="if(!validate_path())return;" Hidden="True"/>
                           </Items>
                        </f:Toolbar>
                    </Toolbars>
                    <Items>
                        <f:Button runat="server" ID="btn_ShowCode" Text="显示单页代码" Icon="PageWhiteCsharp" OnClick="btn_ShowCode_OnClick" Hidden="True" />

                        <f:TabStrip ID="tab2" BoxFlex="1" Margin="0" IsFluid="true" EnableTabCloseMenu="false" runat="server" BodyPadding="5px" ShowBorder="true">
                            <Tools>
                              
                                <f:Tool runat="server" EnablePostBack="false" IconFont="FileCodeO" Text="显示单页代码" ID="Tool_ShowCode">
                                    <Listeners>
                                        <f:Listener Event="click" Handler="onToolShowCodeClick" />
                                    </Listeners>
                                </f:Tool>
                            </Tools>
                             
                            <Tabs>
                                <f:Tab Title="Controller" BodyPadding="10px" Layout="Fit" runat="server" AutoScroll="True">
                                    <Items>
                                        <f:Label runat="server" ID="lb_Controller" EncodeText="False" />
                                    </Items>
                                </f:Tab>
                                <f:Tab Title="Addcshtml" BodyPadding="10px" Layout="Fit" runat="server" AutoScroll="True">
                                    <Items>
                                        <f:Label runat="server" ID="lb_Addcshtml" EncodeText="False" />
                                    </Items>
                                </f:Tab>
                                <f:Tab Title="ListPageCshtml" BodyPadding="10px" Layout="Fit" runat="server" AutoScroll="True">
                                    <Items>
                                        <f:Label runat="server" ID="lb_ListPagecshtml" EncodeText="False" />
                                    </Items>
                                </f:Tab>
                               
                            </Tabs>
                        </f:TabStrip>
                    </Items>

                </f:SimpleForm>



                <f:Panel runat="server" ID="panelRightRegion" RegionPosition="Right" RegionSplit="true" EnableCollapse="true" EnableIFrame="True" IFrameUrl="/Public/DbConfig.aspx"
                    Width="350px" Title="(1) 连接数据库" Layout="Fit" AutoScroll="True" ShowBorder="true" ShowHeader="true" BodyPadding="5px" IconFont="_PullRight">
                </f:Panel>
            </Items>
        </f:Panel>
        <f:HiddenField runat="server" ID="hd_tables" />

    </form>
</body>
</html>
<script type="text/javascript">
    var baseClassName;
    var nameSpace;
    var path;
    
    var savePath;

    var hd_tables;

    F.ready(function () {
        baseClassName = F('<%=txb_BaseClassName.ClientID%>');
        nameSpace = F('<%=txb_NameSpace.ClientID%>');
        path = F('<%=txb_Areas.ClientID%>');
        savePath = F('<%=txb_savePath.ClientID%>');
        hd_tables = F('<%=hd_tables.ClientID%>');

    });

    //表单验证。由于toolbar里不能进行验证所以要js验证
    function validate() {
        var error = "";
        if (baseClassName.getValue().length == 0) {
            baseClassName.markInvalid("请填写[页面继承类名]");
            error += "请填写[页面继承类名]<br/>";
        }
        if (nameSpace.getValue().length == 0) {
            nameSpace.markInvalid("请填写[命名空间]");
            error += "请填写[命名空间]<br/>";
        }
        if (path.getValue().length == 0) {
            path.markInvalid("请填写[项目路径]");
            error += "请填写[项目路径]<br/>";
        }


        if (savePath.getValue().length == 0) {
            savePath.markInvalid("请填写[代码存放位置]");
            error += "请填写[代码存放位置]<br/>";
        }

        //获取表名称
        var tables = $('#Panel1_panelRightRegion iframe')[0].contentWindow.GetTables();
        if (tables.length == 0) {
            error += "请在右侧[数据库]中，选中目标表！";
        }

        if (error.length > 0) {
            F.alert(error);
            return false;
        }

        //取表名称值
        hd_tables.setValue(tables);

        baseClassName.clearInvalid();
        nameSpace.clearInvalid();
        path.clearInvalid();
      
        savePath.clearInvalid();
        return true;
    }

    function validate_path() {
        if (path.getValue().length == 0) {
            path.markInvalid("请填写[项目路径]");
            F.alert("请填写[项目路径]");
            return false;
        } else {
            path.clearInvalid();
            return true;
        }
    }

    function ShowHighlight() {
        $('pre code').each(function (i, block) {
            hljs.highlightBlock(block);
        });
    }

    function validateTable() {
        //获取表名称
        var error = '';
        var tables = $('#Panel1_panelRightRegion iframe')[0].contentWindow.GetTables();
        if (tables.length == 0) {
            error += "请在右侧[数据库]中，选中目标表！";
        }

        if (error.length > 0) {
            F.alert(error);
            return false;
        }

        //取表名称值
        hd_tables.setValue(tables);
        return true;
    }

    function onToolShowCodeClick() {//由于tool不能直接触发点击事件，需要写js
        if(!validate())return ;
        __doPostBack('Panel1$SimpleForm1$btn_ShowCode', '');
    }


</script>
