﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetCode.aspx.cs" Inherits="BoYuanCore.CodeGenerator.FreeSqlClass.SetCode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SqlSugar实体生成</title>
</head>
<body>
    <script src="/Scripts/highlight.pack.js"></script>
    <script>hljs.configure({ useBR: true });/*使用code的自动换行，highlight.js不自行增加换行*/</script>
    <form id="form1" runat="server">
        <f:PageManager runat="server" AutoSizePanelID="Panel1" FormLabelWidth="135px" />
        <f:Panel ID="Panel1" Margin="5px" runat="server" ShowBorder="false" ShowHeader="false" Layout="Region">
            <Items>

                <f:Panel runat="server" ID="panelRightRegion" RegionPosition="Right" RegionSplit="true" EnableCollapse="true" EnableIFrame="True" IFrameUrl="/Public/DbConfig.aspx"
                    Width="350px" Title="(1) 连接数据库" Layout="VBox" ShowBorder="true" ShowHeader="true" BodyPadding="5px" IconFont="_PullUp">
                </f:Panel>

                <f:Panel ID="Panel5" runat="server" ShowBorder="True" Layout="VBox" AutoScroll="true" ShowHeader="False" BoxConfigChildMargin="0 0 5 0" BodyPadding="5">
                    <Items>
                        <f:Form ID="form_Edit" ShowBorder="True" ShowHeader="True" runat="server" AutoScroll="true" Title="(2) 生成整个数据库的表Entity"
                            EnableCollapse="False" BodyPadding="5px 5px" LabelWidth="200px" IsFluid="true">
                            <Rows>
                                <f:FormRow runat="server">
                                    <Items>
                                        <f:Label runat="server" Label="说明" Text="<font color='red'>默认生成代码存放在c盘下，请自行替换到项目中。</font>" EncodeText="False" />
                                    </Items>
                                </f:FormRow>
                                <f:FormRow runat="server">
                                    <Items>
                                        <f:TextBox runat="server" ID="txb_path" Label="代码存放位置" Text="C:\Code\Entity" Required="True" ShowRedStar="True" />
                                    </Items>
                                </f:FormRow>
                                <f:FormRow runat="server">
                                    <Items>
                                        <f:TextBox runat="server" ID="txb_nameSpace" Label="FreeSql实体空间" Text="BoYuanCore.Entities" Required="True" ShowRedStar="True" />
                                    </Items>
                                </f:FormRow>
                                <f:FormRow runat="server">
                                    <Items>
                                        <f:CheckBox runat="server" ID="cb_EntityType" Label="实体类型" DisplayType="Switch" ShowSwitchText="True" SwitchOnText="雪花实体" SwitchOffText="普通实体" Checked="True"/>
                                        <f:Button ID="Button_save" runat="server" ValidateForms="form_Edit" Text="生成代码" Icon="SystemSaveNew" OnClick="Button_save_OnClick" OnClientClick="if(!GetTableInfos())return;" />
                                    </Items>
                                </f:FormRow>
                            </Rows>
                        </f:Form>
                        <f:TabStrip ID="tab2" BoxFlex="1" Margin="0" IsFluid="true" EnableTabCloseMenu="false" runat="server" BodyPadding="5px" ShowBorder="true">
                            <Toolbars>
                                <f:Toolbar runat="server">
                                    <Items>
                                        <f:TextArea runat="server" ID="txb_Expression" Label="表达式" Required="True" ShowRedStar="True" LabelWidth="70px" Width="300px" />
                                        <f:ToolbarSeparator runat="server" />
                                        <f:Button runat="server" ID="Btn_SetSimpleCode" Text="生成单表代码视图" Icon="PageWhiteCsharp" OnClick="Btn_SetSimpleCode_OnClick" ValidateForms="tab2" OnClientClick="if(!GetTableInfos())return;" />
                                        <f:ToolbarSeparator runat="server" /> 
                                        <%--<f:Button ID="Btn_SetCodeFile" runat="server" ValidateForms="form_Edit" Text="表达式批量生成代码" Icon="SystemSaveNew" OnClick="Btn_SetCodeFile_OnClick" OnClientClick="if(!GetTableInfos())return;" />--%>

                                    </Items>
                                </f:Toolbar>
                                <f:Toolbar runat="server">
                                    <Items>
                                        <f:ToolbarText runat="server" Text="<font color='red'>注意:如果输出 “{” 或 “}” 时需要转义，既 “{{” 或 “}}” </font>例如:public string {0} {{get;set;}}<br><font color='red'>占位符含义:</font>{0}:DbColumnNameToClassName  | {1}:ColumnDescription  | {2}:DataType  | {3}:DefaultValue  | {4}:IsPrimarykey  | {5}:IsIdentity  | {6}:IsNullable  | {7}:Length " />
                                    </Items>
                                </f:Toolbar>
                            </Toolbars>
                            <Tabs>
                                <f:Tab Title="获取字段信息，根据表达式代码生成" BodyPadding="10px" Layout="Fit" runat="server" AutoScroll="True">
                                    <Items>
                                        <f:Label runat="server" ID="lb_Select" EncodeText="False" />
                                    </Items>
                                </f:Tab>


                            </Tabs>
                        </f:TabStrip>

                    </Items>

                </f:Panel>

            </Items>
        </f:Panel>
        <f:HiddenField runat="server" ID="hd_tables" />
    </form>
</body>
</html>
<script>
    var hd_tables;
    F.ready(function () {
        hd_tables = F('<%=hd_tables.ClientID%>');
    });

    function GetTableInfos() {
        var error = "";
        //获取表名称
        var tables = $('#Panel1_panelRightRegion iframe')[0].contentWindow.GetTables();
        if (tables.length == 0) {
            error += "请在右侧[数据库]中，选中目标表！";
        }

        if (error.length > 0) {
            F.alert(error);
            return false;
        }

        //取表名称值
        hd_tables.setValue(tables);
        return true;
    }
</script>
