﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DBMigration.aspx.cs" Inherits="BoYuanCore.CodeGenerator.Public.DBMigration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>权限数据初始化</title>
</head>
<body>
    <form id="form1" runat="server">
       <f:PageManager   runat="server" AutoSizePanelID="form_Edit" FormLabelWidth="135px" FormLabelAlign="Right" />
        <f:Form ID="form_Edit" ShowBorder="false" ShowHeader="false" runat="server" AutoScroll="true"
            EnableCollapse="true" BodyPadding="15px 15px">
            <Rows>

                <f:FormRow runat="server">
                    <Items>
                        <f:Label runat="server" EncodeText="False" Text="<a href='https://github.com/dotnetcore/FreeSql/wiki/%E5%85%A5%E9%97%A8' target='_blank'>Freesql连接字符串参考</a> | <a href='https://www.connectionstrings.com/' target='_blank'>数据库连接字符串大全</a>"/>
                       
                    </Items>
                </f:FormRow>
                <f:FormRow runat="server" >
                    <Items>
                        <f:Label runat="server" EncodeText="False" Text="<font color='red'>先确保数据库为空库，否则可能会添加重复数据！</font>"/>
                    </Items>
                </f:FormRow>
                <f:FormRow runat="server" >
                    <Items>
                        <f:TextBox ID="txb_showStr" runat="server" Label="参考数据库连接字符串<br/>(注意有些数据库区分数据库名称大小写)" LabelWidth="200px" LabelAlign="Right" Readonly="True"/>
                    </Items>
                </f:FormRow>
                <f:FormRow runat="server" >
                    <Items>
                        <f:DropDownList runat="server" Label="数据类型" ID="ddl_db" OnSelectedIndexChanged="ddl_db_OnSelectedIndexChanged" AutoPostBack="True">
                            <f:ListItem Text="SqlServer"  Value="SqlServer" Selected="True"/>
                            <f:ListItem Text="MySql"      Value="MySql"      />
                            <f:ListItem Text="Oracle"     Value="Oracle"     />
                            <f:ListItem Text="PostgreSQL" Value="PostgreSQL" />
                        </f:DropDownList>   
                    </Items>
                </f:FormRow>
                <f:FormRow runat="server" >
                    <Items>
                        <f:TextBox runat="server" ID="txb_url" Label="数据库连接" Width="700px" ShowRedStar="True" Required="True"/>
                    </Items>
                </f:FormRow>
                <f:FormRow runat="server" ColumnWidths="100px 100px" >
                    <Items>
                       
                        <f:Button runat="server" Text="测试连接" ID="btn_test" OnClick="btn_test_OnClick" ValidateForms="form_Edit" />
                        <f:Button runat="server" Text="初始化表" ID="btn_DbInit" OnClick="btn_DbInit_OnClick" ValidateForms="form_Edit" />

                    </Items>
                </f:FormRow>
             
               
            </Rows>
          
        </f:Form>
       
    </form>
</body>
</html>
