﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FineUIPro ;

namespace BoYuanCore.CodeGenerator.code
{
    public class CodeFirstPage : FineUIPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //判断是否选择工程
            if (Session["VSProject"] == null)
            {
                Response.Write("请先选择工程");
                Response.End();
            }
        }

        /// <summary>
        /// 字段关系类型
        /// </summary>
        public enum RelationalTypeEnum
        {

            DropDownList      =1,
            CheckBoxList      =2,
            RadioButtonList   =3,
            Tree              =4,

            KindEditor        =21,

            DefalutShowColumn =51,//默认显示字段(例如DropDownList的DataTextField，grid的ExpandUnusedSpace)
            EnumColumn        =52,//枚举字段



            Other = 10000,
        }
    }
}