﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using FineUIPro;

namespace BoYuanCore.CodeGenerator.code
{
    public class FineUIPage: System.Web.UI.Page
    {
        #region 初始化语言和皮肤

        protected override void OnInit(EventArgs e)
        {
            var pm = PageManager.Instance;
            if (pm != null)
            {
                HttpCookie themeCookie = Request.Cookies["Theme"];
                if (themeCookie != null)
                {
                    string themeValue = themeCookie.Value;

                    // 是否为内置主题
                    if (IsSystemTheme(themeValue))
                    {
                        pm.CustomTheme = String.Empty;
                        pm.Theme = (Theme)Enum.Parse(typeof(Theme), themeValue, true);
                    }
                    else
                    {
                        pm.CustomTheme = themeValue;
                    }
                }

                if (Constants.IS_BASE)
                {
                    pm.EnableAnimation = false;
                }
            }

            base.OnInit(e);
        }

        private bool IsSystemTheme(string themeName)
        {
            string[] themes = Enum.GetNames(typeof(Theme));
            foreach (string theme in themes)
            {
                if (theme.Equals(themeName, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion


        #region Fineui公共方法 

        /// <summary>
        /// 显示字体图标
        /// </summary>
        /// <param name="fontIcon">字体图标代码</param>
        /// <returns></returns>
        protected string ShowFontIcon(string fontIcon)
        {
            if (string.IsNullOrEmpty(fontIcon) || fontIcon == "None") return string.Empty;

            IconFont iconType = (IconFont)Enum.Parse(typeof(IconFont), fontIcon);
            string iconName = IconFontHelper.GetName(iconType);

            if (fontIcon.StartsWith("_")) // 以下划线开头的是IconFont字体
            {
                return string.Format("<li class=\"f-icon f-iconfont {0}\"></li>", iconName);
            }
            else
            {
                return string.Format("<li class=\"f-icon f-icon-{0}\"></li>", iconName);
            }
        }


        #region Alert 或 页面跳转

        /// <summary>
        /// 提示信息(弹层用),并指定父页面跳转
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="url">跳转地址</param>
        /// <param name="js">显示消息后 执行的js</param>
        protected void AlertInforAndRedirect(string message, string url, string js = "")
        {
            FineUIPro.Alert aa = new FineUIPro.Alert();
            aa.AlertInforAndRedirect(message, url, js);
        }

        /// <summary>
        /// 提示信息(弹层用)
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        /// <param name="js">显示消息后 执行的js</param>
        protected void AlertInfor(string message, bool isPostBackReference = true, string js = "")
        {
            FineUIPro.Alert aa = new FineUIPro.Alert();
            aa.AlertInfor(message, isPostBackReference, js);
        }

        /// <summary>
        /// 提示信息（单页用 并刷新当前页）
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        /// <param name="js">显示消息后 执行的js</param>
        protected void AlertInforBySingerPage(string message, bool isPostBackReference = true, string js = "")
        {
            FineUIPro.Alert aa = new FineUIPro.Alert();
            aa.AlertInforBySingerPage(message, isPostBackReference, js);

        }

        /// <summary>
        /// 报错信息
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        /// <param name="js">显示消息后 执行的js</param>
        protected void AlertError(string message, bool isPostBackReference = true, string js = "")
        {
            FineUIPro.Alert aa = new FineUIPro.Alert();
            aa.AlertError(message, isPostBackReference, js);
        }

        /// <summary>
        /// 成功信息
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        /// <param name="js">显示消息后 执行的js</param>
        protected void AlertSuccess(string message, bool isPostBackReference = true, string js = "")
        {
            FineUIPro.Alert aa = new FineUIPro.Alert();
            aa.AlertSuccess(message, isPostBackReference, js);
        }

        /// <summary>
        /// 疑问信息
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        /// <param name="js">显示消息后 执行的js</param>
        protected void AlertQuestion(string message, bool isPostBackReference = true, string js = "")
        {
            FineUIPro.Alert aa = new FineUIPro.Alert();
            aa.AlertQuestion(message, isPostBackReference, js);
        }

        /// <summary>
        /// 警告信息
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        /// <param name="js">显示消息后 执行的js</param>
        protected void AlertWarning(string message, bool isPostBackReference = true, string js = "")
        {
            FineUIPro.Alert aa = new FineUIPro.Alert();
            aa.AlertWarning(message, isPostBackReference, js);
        }

        /// <summary>
        /// 子页面关闭并在父业面跳转
        /// </summary>
        /// <param name="url"></param>
        protected void ParentRedirect(string url)
        {
            FineUIPro.Alert aa = new FineUIPro.Alert();
            aa.ParentRedirect(url);
        }

        #endregion

        #region Notify 消息框

        /// <summary>
        /// 信息消息框
        /// </summary>
        /// <param name="message">警告信息</param>
        /// <param name="js">然后执行的js代码</param>
        public void NotifyInformation(string message, string js = null)
        {
            NotifyBase(message, js, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 问题消息框
        /// </summary>
        /// <param name="message">警告信息</param>
        /// <param name="js">然后执行的js代码</param>
        public void NotifyQuestion(string message, string js = null)
        {
            NotifyBase(message, js, MessageBoxIcon.Question);
        }

        /// <summary>
        /// 警告消息框
        /// </summary>
        /// <param name="message">警告信息</param>
        /// <param name="js">然后执行的js代码</param>
        public void NotifyWarning(string message, string js = null)
        {
            NotifyBase(message, js, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// 错误消息框
        /// </summary>
        /// <param name="message">错误信息</param>
        /// <param name="js">然后执行的js代码</param>
        public void NotifyError(string message, string js = null)
        {
            NotifyBase(message, js, MessageBoxIcon.Error);
        }

        /// <summary>
        /// 成功消息框
        /// </summary>
        /// <param name="message">错误信息</param>
        /// <param name="js">然后执行的js代码</param>
        public void NotifySuccess(string message, string js = null)
        {
            NotifyBase(message, js, MessageBoxIcon.Success);
        }

        private void NotifyBase(string message, string js = null, FineUIPro.MessageBoxIcon icon = MessageBoxIcon.Information)
        {
            Notify nf = new Notify();
            nf.GetShowNotify();
            nf.Message = message;
            nf.MessageBoxIcon = icon;
            if (!string.IsNullOrEmpty(js)) nf.HideScript = js;
            nf.Show();
        }

        #endregion

        #region grid


        /// <summary>
        /// 获取选中行的主id
        /// </summary>
        /// <param name="grid">FineUIPro.Grid</param>
        /// <param name="keyNumIndex">第几个主键（从0开始）</param>
        /// <returns></returns>
        public static string GetDataKeysBySelectedRow(FineUIPro.Grid grid, int keyNumIndex = 0)
        {
            return grid.GetDataKeysBySelectedRow(keyNumIndex);
        }

        #endregion

        #region other

        /// <summary>
        /// 显示对或错的图片
        /// </summary>
        /// <param name="tORf"></param>
        /// <returns></returns>
        public static string GetImg_TrueOrFalse(bool tORf)
        {
            string url = FineUIPro.GlobalConfig.GetIconBasePath().Replace("~/", "/") + "/";
            return "<img src='" + url + (tORf ? "Tick" : "cross") + ".png' />";
        }

        #endregion

        /// <summary>
        /// 递归获取页面指定FineUIPro控件
        /// </summary>
        /// <typeparam name="T">控件类型</typeparam>
        /// <param name="controls">当页控件</param>
        /// <param name="idList">其他类型控件，并指定名称集合</param>
        /// <returns></returns>
        public static List<FineUIPro.ControlBase> GetFineUIPermissionsControlList<T>(ControlCollection controls, List<string> idList = null) where T : FineUIPro.ControlBase
        {
            List<FineUIPro.ControlBase> controlList = new List<FineUIPro.ControlBase>();
            if (idList == null)
            {
                foreach (Control ctl in controls)
                {
                    if (ctl is WindowField tempCtl1 && !string.IsNullOrEmpty(tempCtl1.DataIFrameUrlFormatString)) //grid 编辑列
                    {
                        controlList.Add(tempCtl1);
                    }
                    else if (ctl.ID != null && ctl is Button tempCtl) //button id 不能为null
                    {
                        controlList.Add(tempCtl);
                    }

                    var tempList = GetFineUIPermissionsControlList<T>(ctl.Controls, null);
                    if (tempList != null && tempList.Count > 0)
                        controlList = controlList.Concat(tempList).ToList();
                }
            }
            else
            {
                foreach (Control ctl in controls)
                {
                    if (ctl is WindowField tempCtl1 && !string.IsNullOrEmpty(tempCtl1.DataIFrameUrlFormatString)) //grid 编辑列或view列，url不能为null
                    {
                        controlList.Add(tempCtl1);
                    }
                    else if (ctl.ID != null && ctl is Button tempCtl)//button id 不能为null
                    {
                        controlList.Add(tempCtl);
                    }
                    else if (ctl.ID != null && ctl is ControlBase cb) //其他控件id不能null
                    {
                        if (idList.Any(p => p.ToLower().Contains(cb.ID.ToLower())))
                        {
                            controlList.Add(cb);
                        }
                    }

                    var tempList = GetFineUIPermissionsControlList<T>(ctl.Controls, idList);
                    if (tempList != null && tempList.Count > 0)
                        controlList = controlList.Concat(tempList).ToList();
                }
            }

            return controlList;
        }

        /// <summary>
        /// 递归获取页面指定类型的FineUIPro控件
        /// </summary>
        /// <param name="controls">当页控件</param>
        /// <returns></returns>
        public static List<FineUIPro.ControlBase> GetFineUIControlList(ControlCollection controls)
        {
            List<FineUIPro.ControlBase> controlList = new List<FineUIPro.ControlBase>();

            foreach (Control ctl in controls)
            {
                if (ctl is ControlBase tempCtl)
                {
                    controlList.Add(tempCtl);
                }

                var tempList = GetFineUIControlList(ctl.Controls);
                if (tempList != null && tempList.Count > 0)
                    controlList = controlList.Concat(tempList).ToList();
            }
            return controlList;
        }

        #endregion
    }
}