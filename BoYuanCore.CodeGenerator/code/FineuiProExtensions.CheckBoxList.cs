﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using FineUIPro;
using FreeSql;

namespace FineUIPro
{
    public static partial class FineUIProExtensions
    {


        /// <summary>
        /// 绑定枚举类型到复选框 //Bind(newstype, typeof (EnumClass .NewsType));
        /// </summary>
        /// <param name="cbl"> 复选框</param>
        /// <param name="enumType"> 枚举类型</param>
        /// <param name="isOnlyBindText">是否只绑定Key(文本)</param>
        /// <param name="defaultSelected">默认选中状态</param>
        public static void Bind(this FineUIPro.CheckBoxList cbl, Type enumType, bool isOnlyBindText = false,bool defaultSelected=false)
        {
            if (isOnlyBindText)
            {
                foreach (var name in Enum.GetNames(enumType))
                {
                    cbl.Items.Add(new FineUIPro.CheckItem(name, name, defaultSelected)); 
                }
            }
            else
            {
                foreach (var item in Enum.GetValues(enumType))
                {
                    cbl.Items.Add(new FineUIPro.CheckItem(item.ToString(), ((int)item).ToString(), defaultSelected));
                }
            }
        }

        /// <summary>
        /// 绑定复选框
        /// </summary>
        /// <param name="cbl">控件</param>
        /// <param name="ds">数据源</param>
        /// <param name="textName">文本字段</param>
        /// <param name="valueName">值字段</param>
        public static void BindByDataSource(this FineUIPro.CheckBoxList cbl, object ds, string textName, string valueName)
        {
            cbl.DataTextField = textName;
            cbl.DataValueField = valueName;
            cbl.DataSource = ds;
            cbl.DataBind();
        }

        /// <summary>
        /// 获取选中文本
        /// </summary>
        /// <param name="cbl">复选框</param>
        /// <returns></returns>
        public static string GetTexts(this FineUIPro.CheckBoxList cbl)
        {
            StringBuilder sb = new StringBuilder();
            foreach (FineUIPro.CheckItem item in cbl.SelectedItemArray)
            {
                sb.AppendFormat("{0},", item.Text);
            }
            return sb.ToString().TrimEnd(',');
        }

        /// <summary>
        /// 获取选中值
        /// </summary>
        /// <param name="cbl">复选框</param>
        /// <returns></returns>
        public static string GetValues(this FineUIPro.CheckBoxList cbl)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string item in cbl.SelectedValueArray)
            {
                sb.AppendFormat("{0},", item);
            }
            return sb.ToString().TrimEnd(',');
        }


        /// <summary>
        /// 获取选中值集合(注意，选中项值为int类型，否则转换异常！)
        /// </summary>
        /// <param name="cbl">复选框</param>
        /// <returns></returns>
        public static int[] GetIntValues(this FineUIPro.CheckBoxList cbl)
        {
            return Array.ConvertAll<string, int>(cbl.SelectedValueArray, int.Parse);
        }


    }
}


