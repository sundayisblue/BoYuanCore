﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using FineUIPro;
using FreeSql;

namespace FineUIPro
{
    public static partial class FineUIProExtensions
    {

      

        /// <summary>
        /// 绑定枚举类型到下拉框 //Bind(newstype, typeof (EnumClass .NewsType));
        /// </summary>
        /// <param name="dll"> 下拉框</param>
        /// <param name="enumType"> 枚举类型</param>
        /// <param name="isOnlyBindText">是否只绑定Key(文本)</param>
        public static void Bind(this FineUIPro.DropDownList dll, Type enumType, bool isOnlyBindText = false)
        {
            if (isOnlyBindText)
            {
                foreach (var name in Enum.GetNames(enumType))
                {
                    dll.Items.Add(new FineUIPro.ListItem(name, name));
                }
            }
            else
            {
                //foreach (var name in Enum.GetNames(enumType))
                //{
                //    dll.Items.Add(new FineUIPro.ListItem(name,(int)Enum.Parse(enumType,name,true)+""));
                //}
                foreach (var item in Enum.GetValues(enumType))
                {
                    dll.Items.Add(new FineUIPro.ListItem(item.ToString(), ((int)item).ToString()));
                }
            }
        }

        /// <summary>
        /// 绑定下拉框
        /// </summary>
        /// <param name="dll">控件</param>
        /// <param name="ds">数据源</param>
        /// <param name="textName">文本字段</param>
        /// <param name="valueName">值字段</param>
        public static void BindByDataSource(this FineUIPro.DropDownList dll, object ds, string textName, string valueName)
        {
            dll.DataTextField = textName;
            dll.DataValueField = valueName;
            dll.DataSource = ds;
            dll.DataBind();

            //dll.Items.Insert(0,new FineUIPro.ListItem("全部","0"));//添加 '全部'到第一行。
        }

        /// <summary>
        /// 获取可编辑下拉框的文本值
        /// </summary>
        /// <param name="dll"></param>
        /// <returns></returns>
        public static string GetEditText(this FineUIPro.DropDownList dll)
        {
            //注意，如果dll的item是null(一个item都没有绑定)，dll.Text的值永远是空字符串
            return dll.SelectedItem != null ? dll.SelectedItem.Text : dll.Text;
        }


        /// <summary>
        /// 获取多选值
        /// </summary>
        /// <param name="dll"></param>
        public static List<string> GetSelectionValues(this FineUIPro.DropDownList dll)
        {
            if (dll.SelectedItem != null)
            {
                //List<string> texts = new List<string>();
                List<string> values = new List<string>();
                foreach (ListItem item in dll.SelectedItemArray)
                {
                    //texts.Add(item.Text);
                    values.Add(item.Value);
                }

                return values;

               
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 获取多选文本
        /// </summary>
        /// <param name="dll"></param>
        public static List<string> GetSelectionTests(this FineUIPro.DropDownList dll)
        {
            if (dll.SelectedItem != null)
            {
                List<string> texts = new List<string>();
                //List<string> values = new List<string>();
                foreach (ListItem item in dll.SelectedItemArray)
                {
                    texts.Add(item.Text);
                    //values.Add(item.Value);
                }

                return texts;

               
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 根据值集合设置多选状态
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="list"></param>
        public static void SetSelectionByValues(this FineUIPro.DropDownList ddl,IEnumerable<string> list)
        {
            foreach (var v in list)
            {
                foreach (var t in ddl.Items)
                {
                    if (t.Value == v)
                    {
                        t.Selected =true;
                    }
                    //不要设置t.Selected = false; 否则会导致多选中项失效
                    //else
                    //{
                    //    t.Selected = false;
                    //}

                }
            }
        }


    }

}
