﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FineUIPro;
using FreeSql;

namespace FineUIPro
{
    public static partial class FineUIProExtensions
    {
        /// <summary>
        /// 绑定枚举类型到单选框 //Bind(newstype, typeof (EnumClass .NewsType));
        /// </summary>
        /// <param name="rbl"> 单选框</param>
        /// <param name="enumType"> 枚举类型</param>
        /// <param name="isOnlyBindText">是否只绑定Key(文本)</param>
        public static void Bind(this FineUIPro.RadioButtonList rbl, Type enumType, bool isOnlyBindText = false)
        {
            if (isOnlyBindText)
            {
                foreach (var name in Enum.GetNames(enumType))
                {
                    rbl.Items.Add(new FineUIPro.RadioItem(name, name));
                }
            }
            else
            {
                foreach (var item in Enum.GetValues(enumType))
                {
                    rbl.Items.Add(new FineUIPro.RadioItem(item.ToString(), ((int)item).ToString()));
                }
            }
        }

        /// <summary>
        /// 绑定单选框
        /// </summary>
        /// <param name="rbl">控件</param>
        /// <param name="ds">数据源</param>
        /// <param name="textName">文本字段</param>
        /// <param name="valueName">值字段</param>
        public static void BindByDataSource(this FineUIPro.RadioButtonList rbl, object ds, string textName, string valueName)
        {
            rbl.DataTextField = textName;
            rbl.DataValueField = valueName;
            rbl.DataSource = ds;
            rbl.DataBind();
        }
    }

}


