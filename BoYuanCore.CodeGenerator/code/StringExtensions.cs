﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace BoYuanCore.CodeGenerator.code
{
    public static class StringExtensions
    {
        /// <summary>
        /// 忽略大小写 是否包含指定的字符串
        /// </summary>
        /// <param name="source"></param>
        /// <param name="toCheck"></param>
        /// <returns></returns>
        public static bool ContainsByIgnoreCase(this string source, string toCheck)
        {
            return source.IndexOf(toCheck, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        /// <summary>
        /// 忽略大小写 是否包含指定的字符串
        /// </summary>
        /// <param name="sourceList"></param>
        /// <param name="toCheck"></param>
        /// <returns></returns>
        public static bool ContainsByIgnoreCase(this IEnumerable<string> sourceList, string toCheck)
        {
            return sourceList.Any(p => ContainsByIgnoreCase((string)p, toCheck));
        }
        /// <summary>
        /// 从头开始截取最大长度的字符串
        /// </summary>
        /// <param name="source"></param>
        /// <param name="maxLength">长度(大于0的长度)</param>
        /// <returns></returns>
        public static string ToMaxLengthString(this string source, int maxLength)
        {
            if (maxLength < 1) throw new Exception("ToMaxLengthString方法中maxLength值必须大于0");
            return source.Length > maxLength ? source.Substring(0, maxLength) : source;
        }

        /// <summary>
        /// 修改第一个字符为大写
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToUpperInitial(this string source)
        {
            if (string.IsNullOrEmpty(source)) return source;
            return source.Substring(0, 1).ToUpper() + source.Substring(1);
        }
        /// <summary>
        /// 修改第一个字符为小写
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToLowerInitial(this string source)
        {
            if (string.IsNullOrEmpty(source)) return source;
            return source.Substring(0, 1).ToLower() + source.Substring(1);
        }

        /// <summary>
        /// 按字符串(或表达式)分隔字符串数组
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="separatorString">分隔符:字符串(或表达式)</param>
        /// <param name="regexOptions">忽略大小写选项等</param>
        /// <returns></returns>
        public static string[] SplitByString(this string input, string separatorString, RegexOptions regexOptions = RegexOptions.IgnoreCase)
        {
            return Regex.Split(input, separatorString, regexOptions);
        }

        /// <summary>
        /// 忽略大小写进行替换字符串
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="pattern">要替换的样式字符串</param>
        /// <param name="replacement">替换后的字符串</param>
        /// <returns></returns>
        public static string ReplaceStringIgnoreCase(this string input, string pattern, string replacement)
        {
            return Regex.Replace(input, pattern, replacement, RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// 换行符(老记不住,故封装一个)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string LineBreak(this string input)
        {
            return input + System.Environment.NewLine;
        }

    }
}