﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace BoYuanCore.CodeGenerator.code
{
    /// <summary>
    /// 字符串相关工具类
    /// </summary>
    public class StringHelper
    {
        /// <summary>
        /// 生成数字随机数
        /// </summary>
        /// <param name="lengthNum">数字长度</param>
        /// <returns></returns>
        public static string GetRandomNum(int lengthNum)
        {
            string a = "0123456789";
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < lengthNum; i++)
            {
                sb.Append(a[new Random(Guid.NewGuid().GetHashCode()).Next(0, a.Length - 1)]);
            }

            return sb.ToString();
        }

        /// <summary>
        /// 获取两个字符串中间的字符串
        /// </summary>
        /// <param name="str">要处理的字符串,例ABCD</param>
        /// <param name="str1">第1个字符串,例AB</param>
        /// <param name="str2">第2个字符串,例D</param>
        /// <param name="isIgnoreCase">是否忽略大小</param>
        /// <returns>例返回C</returns>
        public static string GetBetweenStr(string str, string str1, string str2, bool isIgnoreCase = true)
        {
            int i1 = isIgnoreCase ? str.IndexOf(str1, StringComparison.OrdinalIgnoreCase) : str.IndexOf(str1);
            if (i1 < 0) //找不到返回空
            {
                return string.Empty;
            }

            int i2 = isIgnoreCase ? str.IndexOf(str2, i1 + str1.Length, StringComparison.OrdinalIgnoreCase) : str.IndexOf(str2, i1 + str1.Length); //从找到的第1个字符串后再去找
            if (i2 < 0) //找不到返回空
            {
                return string.Empty;
            }

            return str.Substring(i1 + str1.Length, i2 - i1 - str1.Length);
        }

        /// <summary>
        /// 替换字符串中间多余(包括tab)的空格
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ReplaceExcessSpace(string str)
        {
            return Regex.Replace(str, @"[ ]+|\t", " ");
        }

        /// <summary>
        /// 根据分割符号，获取key和value。例如:分割符 : ,Referer: https://123.com 获取 Referer，和https://123.com
        /// </summary>
        /// <param name="info"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="indexOfChar">分割符号</param>
        public static void GetKeyAndValue(string info, out string key, out string value, string indexOfChar = "=")
        {
            int startIndex = info.IndexOf(indexOfChar);//获取第一个等号的位置
            if (startIndex < 0)
            {
                key = string.Empty;
                value = string.Empty;
                return;
            }

            key = info.Substring(0, startIndex);//获取分组最后一个字符串，就是key
            value = info.Substring(startIndex + indexOfChar.Length);
        }


        #region 转换人民币大小金额
        /// <summary> 
        /// 转换人民币大小金额 
        /// </summary> 
        /// <param name="num">金额</param> 
        /// <returns>返回大写形式</returns> 
        public static string CmycurD(decimal num)
        {
            string str1 = "零壹贰叁肆伍陆柒捌玖";            //0-9所对应的汉字 
            string str2 = "万仟佰拾亿仟佰拾万仟佰拾元角分"; //数字位所对应的汉字 
            string str3 = string.Empty;    //从原num值中取出的值 
            string str4 = string.Empty;    //数字的字符串形式 
            string str5 = string.Empty;  //人民币大写金额形式 
            int i;    //循环变量 
            int j;    //num的值乘以100的字符串长度 
            string ch1 = string.Empty;    //数字的汉语读法 
            string ch2 = string.Empty;    //数字位的汉字读法 
            int nzero = 0;  //用来计算连续的零值是几个 
            int temp;            //从原num值中取出的值 

            num = Math.Round(Math.Abs(num), 2);    //将num取绝对值并四舍五入取2位小数 
            str4 = ((long)(num * 100)).ToString();        //将num乘100并转换成字符串形式 
            j = str4.Length;      //找出最高位 
            if (j > 15) { return "溢出"; }
            str2 = str2.Substring(15 - j);   //取出对应位数的str2的值。如：200.55,j为5所以str2=佰拾元角分 

            //循环取出每一位需要转换的值 
            for (i = 0; i < j; i++)
            {
                str3 = str4.Substring(i, 1);          //取出需转换的某一位的值 
                temp = Convert.ToInt32(str3);      //转换为数字 
                if (i != (j - 3) && i != (j - 7) && i != (j - 11) && i != (j - 15))
                {
                    //当所取位数不为元、万、亿、万亿上的数字时 
                    if (str3 == "0")
                    {
                        ch1 = string.Empty;
                        ch2 = string.Empty;
                        nzero = nzero + 1;
                    }
                    else
                    {
                        if (str3 != "0" && nzero != 0)
                        {
                            ch1 = "零" + str1.Substring(temp * 1, 1);
                            ch2 = str2.Substring(i, 1);
                            nzero = 0;
                        }
                        else
                        {
                            ch1 = str1.Substring(temp * 1, 1);
                            ch2 = str2.Substring(i, 1);
                            nzero = 0;
                        }
                    }
                }
                else
                {
                    //该位是万亿，亿，万，元位等关键位 
                    if (str3 != "0" && nzero != 0)
                    {
                        ch1 = "零" + str1.Substring(temp * 1, 1);
                        ch2 = str2.Substring(i, 1);
                        nzero = 0;
                    }
                    else
                    {
                        if (str3 != "0" && nzero == 0)
                        {
                            ch1 = str1.Substring(temp * 1, 1);
                            ch2 = str2.Substring(i, 1);
                            nzero = 0;
                        }
                        else
                        {
                            if (str3 == "0" && nzero >= 3)
                            {
                                ch1 = string.Empty;
                                ch2 = string.Empty;
                                nzero = nzero + 1;
                            }
                            else
                            {
                                if (j >= 11)
                                {
                                    ch1 = string.Empty;
                                    nzero = nzero + 1;
                                }
                                else
                                {
                                    ch1 = string.Empty;
                                    ch2 = str2.Substring(i, 1);
                                    nzero = nzero + 1;
                                }
                            }
                        }
                    }
                }
                if (i == (j - 11) || i == (j - 3))
                {
                    //如果该位是亿位或元位，则必须写上 
                    ch2 = str2.Substring(i, 1);
                }
                str5 = str5 + ch1 + ch2;

                if (i == j - 1 && str3 == "0")
                {
                    //最后一位（分）为0时，加上“整” 
                    str5 = str5 + '整';
                }
            }
            if (num == 0)
            {
                str5 = "零元整";
            }
            return str5;
        }
        #endregion

    }
}