﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="BoYuanCore.CodeGenerator._default" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>BoYuanCore 代码生成工具</title>
  
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager ID="PageManager1" AutoSizePanelID="regionPanel" runat="server" />
        <f:Panel ID="regionPanel" Layout="Region" ShowBorder="false" ShowHeader="false" runat="server">
            <Items>
              
                <f:Panel ID="leftPanel" EnableCollapse="True" Width="200px" RegionSplit="true" RegionSplitIcon="false" RegionSplitWidth="3px"
                    ShowHeader="false" Title="功能列表" Layout="Fit" RegionPosition="Left"   runat="server">
                    <Items>
                        <f:Tree runat="server" ID="panelLeftRegion" RegionPosition="Left" RegionSplit="true" EnableCollapse="False" 
                            Width="250px" Title="功能列表" ShowBorder="true" ShowHeader="true" BodyPadding="5px"  >
                            <Nodes>
                                <f:TreeNode Text="代码生成(DbFirst)" Expanded="true" NodeID="CodeGenerator" >
                                    <f:TreeNode Text="FineuiCore" NodeID="FineuiCore" NavigateUrl="FineuiCore/SetFineuiCoreCode.aspx" Expanded="true"  EnableClickEvent="true"/>
                                    <f:TreeNode Text="FreeSql" NodeID="FreeSql" NavigateUrl="FreeSql/SetCode.aspx" Expanded="true"  EnableClickEvent="true"/>
                                    <f:TreeNode Text="权限表基础创建" NodeID="DBMigration" NavigateUrl="Public/DBMigration.aspx" Expanded="true"  EnableClickEvent="true"/>
                                    <f:TreeNode Text="Webform转Mvc" NodeID="webformtomvc" NavigateUrl="FineuiCore/FineUIToMvc.aspx" Expanded="true"  EnableClickEvent="true"/>
                                    <%--<f:TreeNode Text="数据库连接" NodeID="DbConfig" NavigateUrl="DbConfig.aspx" Expanded="true"  EnableClickEvent="true"/>--%>
                                </f:TreeNode>

                                <f:TreeNode Text="其他设置" Expanded="false" NodeID="other" >
                                    <f:TreeNode Text="主题" NodeID="theme" NavigateUrl="FineuiCore/theme.aspx" Expanded="true" EnableClickEvent="true"/>
                                </f:TreeNode>
                            </Nodes>
                        </f:Tree>
                    </Items>
                </f:Panel>
                <f:TabStrip ID="mainTabStrip" RegionPosition="Center" EnableTabCloseMenu="true" ShowBorder="false" runat="server">
                    <Tabs>
    
                        <f:Tab Title="使用示例" Icon="House" runat="server" IFrameUrl="Public/CodeHelp/index.html" EnableIFrame="True">
                            <Items>
                            <f:Label runat="server" EncodeText="false" Text="&nbsp;&nbsp;&nbsp;欢迎使用"/>
                            </Items>
                        </f:Tab>
                    </Tabs>
                    <Tools>
                        <f:Tool runat="server" EnablePostBack="false" IconFont="_Refresh" ToolTip="刷新本页" ID="toolRefresh">
                            <Listeners>
                                <f:Listener Event="click" Handler="onToolRefreshClick" />
                            </Listeners>
                        </f:Tool>
                        <f:Tool runat="server" EnablePostBack="false" IconFont="_Maximize" ToolTip="最大化" ID="toolMaximize">
                            <Listeners>
                                <f:Listener Event="click" Handler="onToolMaximizeClick" />
                            </Listeners>
                        </f:Tool>
                        <f:Tool runat="server" EnablePostBack="false" IconFont="_NewTab" ToolTip="在新标签页中打开" ID="toolNewWindow">
                            <Listeners>
                                <f:Listener Event="click" Handler="onToolNewWindowClick" />
                            </Listeners>
                        </f:Tool>
                    </Tools>
                </f:TabStrip>
            </Items>
        </f:Panel>
        <f:Window ID="Window1" runat="server" IsModal="true" Hidden="true" EnableIFrame="true"
            EnableResize="true" EnableMaximize="true" IFrameUrl="edit.aspx" Width="800px"
            Height="500px">
        </f:Window>
       
    </form>
    <script>
  
        var editWindow = '<%= Window1.ClientID %>';
        var mainTabStripClientID = '<%= mainTabStrip.ClientID %>';
       
        var leftPanelClientID = '<%= leftPanel.ClientID %>';


        // 点击标题栏工具图标 - 刷新
        function onToolRefreshClick(event) {
            var mainTabStrip = F(mainTabStripClientID);

            var activeTab = mainTabStrip.getActiveTab();
            if (activeTab.iframe) {
                var iframeWnd = activeTab.getIFrameWindow();
                iframeWnd.location.reload();
            }
        }

        // 点击标题栏工具图标 - 最大化
        function onToolMaximizeClick() {
            var leftPanel = F(leftPanelClientID);
            var currentTool = this;
            F.noAnimation(function () {
                if (currentTool.iconFont === 'f-iconfont-maximize') {
                    currentTool.setIconFont('f-iconfont-restore');
                    leftPanel.collapse();
                } else {
                    currentTool.setIconFont('f-iconfont-maximize');
                    leftPanel.expand();
                }
            });
        }

        //菜单样式
        function setMenuStyle(styleVal) {
            //accordion
            F.cookie('MenuStyle', styleVal, {
                expires: 100  // 单位：天
            });

            top.window.location.reload();
        }
        // 点击标题栏工具图标 - 在新标签页中打开
        function onToolNewWindowClick(event) {
            var mainTabStrip = F(mainTabStripClientID);

            var activeTab = mainTabStrip.getActiveTab();
            if (activeTab.iframe) {
                var iframeUrl = activeTab.getIFrameUrl();
                iframeUrl = iframeUrl.replace(/\/mobile\/\?file=/ig, '/mobile/');
                window.open(iframeUrl, '_blank');
            }
        }


        // 添加示例标签页
        // pid： 选项卡ID
        // iframeUrl: 选项卡IFrame地址 
        // title： 选项卡标题
        // icon： 选项卡图标
        // createToolbar： 创建选项卡前的回调函数（接受tabOptions参数）
        // refreshWhenExist： 添加选项卡时，如果选项卡已经存在，是否刷新内部IFrame
        // iconFont： 选项卡图标字体
        function addExampleTab(tabOptions) {

            if (typeof (tabOptions) === 'string') {
                tabOptions = {
                    id: arguments[0],
                    iframeUrl: arguments[1],
                    title: arguments[2],
                    icon: arguments[3],
                    createToolbar: arguments[4],
                    refreshWhenExist: arguments[5],
                    iconFont: arguments[6]
                };
            }

            F.addMainTab(F(mainTabStripClientID), tabOptions);
        }


        // 移除选中标签页
        function removeActiveTab() {
            var mainTabStrip = F(mainTabStripClientID);

            var activeTab = mainTabStrip.getActiveTab();
            activeTab.hide();
        }


        F.ready(function () {

            var mainTabStrip = F(mainTabStripClientID);
            var leftPanel = F(leftPanelClientID);
            var treeMenu = leftPanel.getItem(0);

            // 初始化主框架中的树(或者Accordion+Tree)和选项卡互动，以及地址栏的更新
            // treeMenu： 主框架中的树控件实例，或者内嵌树控件的手风琴控件实例
            // mainTabStrip： 选项卡实例
            // updateHash: 切换Tab时，是否更新地址栏Hash值（默认值：true）
            // refreshWhenExist： 添加选项卡时，如果选项卡已经存在，是否刷新内部IFrame（默认值：false）
            // refreshWhenTabChange: 切换选项卡时，是否刷新内部IFrame（默认值：false）
            // maxTabCount: 最大允许打开的选项卡数量
            // maxTabMessage: 超过最大允许打开选项卡数量时的提示信息
            F.initTreeTabStrip(treeMenu, mainTabStrip, {
                //maxTabCount: 10,
                //maxTabMessage: '请先关闭一些选项卡（最多允许打开 10 个）！'
                updateHash:false
            });
        });

    </script>
</body>
</html>
