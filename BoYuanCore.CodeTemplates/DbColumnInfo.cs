﻿using System;
using System.Collections.Generic;
using System.Text;
using BoYuanCore.CoreCodeTemplates.DBMappingRules;

namespace BoYuanCore.CoreCodeTemplates
{
    public class DbColumnInfo
    {
        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 表名对应的类名称(首字母大写)
        /// </summary>
        public string TableNameToClassName => DataTypeMapping.GetFormatName(TableName); //首字母大写

        /// <summary>
        /// 列名称
        /// </summary>
        public string DbColumnName{ get; set; }

        /// <summary>
        /// 列名称对应的属性名称(首字母大写)
        /// </summary>
        public string DbColumnNameToClassName => DataTypeMapping.GetFormatName(DbColumnName); //首字母大写

        /// <summary>
        /// 映射到 C# 类型
        /// </summary>
        public Type CsType { get; set; }//PropertyType
        /// <summary>
        /// 自增标识
        /// </summary>
        public bool IsIdentity { get; set; }
        /// <summary>
        /// 是否可DBNull
        /// </summary>
        public bool IsNullable { get; set; }

        /// <summary>
        /// 字段类型(string ,int 等),默认就是小写
        /// </summary>
        public string PropertyName => DataTypeMapping.Csharp.GetCsharpKeyword(CsType.Name).ToLower();

        /// <summary>
        /// 最大长度(如果nvarchar(max) 长度为-1)
        /// </summary>
        public int MaxLength { get; set; }
        /// <summary>
        /// 字段备注
        /// </summary>
        public string Coment { get; set; }
        
        /// <summary>
        /// 默认值
        /// </summary>
        public string DefaultValue { get; set; }
       
        /// <summary>
        /// 是否为主键
        /// </summary>
        public bool IsPrimaryKey { get; set; }
       
        /// <summary>
        /// 数据库字段类型，字符串，varchar
        /// </summary>
        public string DbTypeText { get; set; }//DataType
        /// <summary>
        /// 数据库字段类型，字符串，varchar(255)
        /// </summary>
        public string DbTypeTextFull { get; set; }

    }
}
