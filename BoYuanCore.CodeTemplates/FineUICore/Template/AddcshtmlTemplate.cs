﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoYuanCore.CoreCodeTemplates.DBMappingRules;
using BoYuanCore.CoreCodeTemplates.FineUICore;


namespace BoYuanCore.CoreCodeTemplates.FineUICore.Template
{
    public class AddcshtmlTemplate: FineUIBase
    {
        public static string GetCode(TableModel tb)
        {
            List<DbColumnInfo> list = tb.Columns;
            DbColumnInfo keyModel = list.FirstOrDefault(p => p.IsPrimaryKey == true);
            if (keyModel == null) { throw new Exception("表没有主键，没办法生成代码！ControllerTemplate"); }

            string defaultValueStr = string.Empty;
            string  formRowStr= GetFormRowCode(tb.Columns, ref defaultValueStr);

            string code = string.Format(@"
@{{
    ViewBag.Title = ""编辑页面"";
    var F = Html.F();
}}

@model {0}.{1}

@section head {{

}}

@section body {{
    @(F.Form().ID(""form_Edit"").ShowHeader(false).ShowBorder(false).AutoScroll(true).BodyPadding(""15px 15px"").IsViewPort(true)
        .Rows(
{3}           
        )
        .Toolbars(F.Toolbar().Position(ToolbarPosition.Bottom)
            .Items(F.HiddenFieldFor(m=>m.{2}).ID(""f_{2}""), F.ToolbarFill(),
                F.Button().ID(""BtnSave"").ValidateForms(""form_Edit"").Text(""保存"").Icon(Icon.SystemSaveNew).OnClientClick(""AddInfo()"")
            )
        ))
}}

@section script {{
     <script>
        F.ready(function () {{
            @if(ViewBag.haveUpdate == null)
            {{
{4}
            }}

            @if(ViewBag.InitLoadJs != null)
            {{
                @Html.Raw(ViewBag.InitLoadJs)
            }}
        }})

        function AddInfo() {{
            F.doPostBack('@Url.Action(""BtnSave_OnClick"")', ""form_Edit"");
        }}
     </script>
}}
"
                , tb.ModelName               //0
                , tb.TableNameToClassName               //1
                , (!keyModel.IsIdentity && keyModel.PropertyName=="long") ?"ID":keyModel.DbColumnNameToClassName      //2 非自增long类型主键为雪花ID
                , formRowStr                 //3
                , defaultValueStr            //4  

            );

            return code;
        }

        private static string GetFormRowCode(List<DbColumnInfo> clms, ref string defaultValueStr)
        {
            var list = clms.Where(p => p.IsPrimaryKey == false).ToList();
            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();//生成初始化默认值的代码。
            string f_control_Temp = string.Empty;
            string tempDefaultValueStr = string.Empty;
            for (int i = 0; i < list.Count; i++)
            {
                f_control_Temp = GetFormControl(list[i],ref tempDefaultValueStr);
                sb2.Append(tempDefaultValueStr);
                if (f_control_Temp.Length > 0)
                {
                    sb.AppendLine("             ,F.FormRow().Items(" + f_control_Temp+")");
                }
            }

            defaultValueStr = sb2.ToString();
            return "             " + (sb.ToString().TrimStart().TrimStart(','));
        }

        private static string GetFormControl(DbColumnInfo clm,ref string defaultValueStr)
        {
            string showStr = !clm.IsNullable ? ".Required(true).ShowRedStar(true)" : string.Empty;//是否必填，0不可以为null，1可以为null
            string showName = GetComment(clm.Coment, clm.DbColumnNameToClassName);
            string showDefault = GetDefaultValue(clm.DefaultValue);
            defaultValueStr = string.Empty;
            
            switch (clm.PropertyName) 
            {
                case "bool":
                    return "F.CheckBoxFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName + "\").Label(\"" + showName + "\").DisplayType(CheckBoxDisplayType.Switch).SwitchTextVisible(true)";


                case "datetime":
                    if (!string.IsNullOrEmpty(clm.DefaultValue))
                    {
                        return string.Empty;
                    }
                    else
                    {
                        return "F.DatePickerFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName +
                               "\").DateFormatString(\"yyyy-MM-dd HH:mm:ss\").Label(\"" + showName + "\")" + showStr;
                    }

                //长类型，由于js不支持太长的数字，故不做最大和最小值约束
                case "uint"://break;
                case "long":
                case "ulong":
                    defaultValueStr = "                @Html.Raw(\"F.ui.f_" + clm.DbColumnNameToClassName + ".setValue('" + showDefault + "');\");" + Environment.NewLine;
                    return "F.NumberBoxFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName + "\").Label(\"" + showName + "\")" + showStr + ".NoDecimal(true)";

                //短整型
                case "byte":
                    defaultValueStr = "                @Html.Raw(\"F.ui.f_" + clm.DbColumnNameToClassName + ".setValue('" + showDefault + "');\");" + Environment.NewLine;
                    return "F.NumberBoxFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName + "\").Label(\"" + showName + "\")" + showStr + ".NoDecimal(true).MaxValue(" + byte.MaxValue + ").MinValue(" + byte.MinValue + ")";

                case "sbyte":
                    defaultValueStr = "                @Html.Raw(\"F.ui.f_" + clm.DbColumnNameToClassName + ".setValue('" + showDefault + "');\");" + Environment.NewLine;
                    return "F.NumberBoxFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName + "\").Label(\"" + showName + "\")" + showStr + ".NoDecimal(true).MaxValue(" + sbyte.MaxValue + ").MinValue(" + sbyte.MinValue + ")";
                case "short":
                    defaultValueStr = "                @Html.Raw(\"F.ui.f_" + clm.DbColumnNameToClassName + ".setValue('" + showDefault + "');\");" + Environment.NewLine;
                    //由于js对长数值判断有异常，故不设置MaxValue
                    //return "F.NumberBoxFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName + "\").Label(\"" + showName + "\")" + showStr + ".NoDecimal(true).MaxValue(" + short.MaxValue + ").MaxValue(" + short.MinValue + ")";
                    return "F.NumberBoxFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName + "\").Label(\"" + showName + "\")" + showStr + ".NoDecimal(true)";
                case "ushort":
                    defaultValueStr = "                @Html.Raw(\"F.ui.f_" + clm.DbColumnNameToClassName + ".setValue('" + showDefault + "');\");" + Environment.NewLine;
                    //由于js对长数值判断有异常，故不设置MaxValue
                    //return "F.NumberBoxFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName + "\").Label(\"" + showName + "\")" + showStr + ".NoDecimal(true).MaxValue(" + ushort.MaxValue + ").MaxValue(" + ushort.MinValue + ")";
                    return "F.NumberBoxFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName + "\").Label(\"" + showName + "\")" + showStr + ".NoDecimal(true)";
                case "enum"://break;
                case "int":
                    defaultValueStr = "                @Html.Raw(\"F.ui.f_" + clm.DbColumnNameToClassName + ".setValue('" + showDefault + "');\");" + Environment.NewLine;
                    //由于js对长数值判断有异常，故不设置MaxValue
                    //return "F.NumberBoxFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName + "\").Label(\"" + showName + "\")" + showStr + ".NoDecimal(true).MaxValue("+int.MaxValue+ ").MinValue(" + int.MinValue + ")";
                    return "F.NumberBoxFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName + "\").Label(\"" + showName + "\")" + showStr + ".NoDecimal(true)";
                  
                //长浮点型, 由于js不支持太长的数字，故不做最大和最小值约束
                case "decimal"://break;
                case "float"://break;
                case "double":
                    defaultValueStr = "                @Html.Raw(\"F.ui.f_" + clm.DbColumnNameToClassName + ".setValue('" + showDefault + "');\");" + Environment.NewLine;
                    return "F.NumberBoxFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName + "\").Label(\"" + showName + "\")" + showStr + ".NoDecimal(false)";

                case "guid"://break;
                default:
                    if (clm.PropertyName== "string") //字符串类型
                    {
                        if (clm.MaxLength > 0) //TextBox
                        {
                            return "F.TextBoxFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName + "\").Label(\"" + showName + "\")" + showStr + ".MaxLength(" + clm.MaxLength + ")";
                        }
                        else //TextArea
                        {
                            return "F.TextAreaFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName + "\").Label(\"" + showName + "\")" + showStr + ".Height(100)";
                        }
                    }
                    else //未知类型
                    {
                        return "F.TextBoxFor(m=>m." + clm.DbColumnNameToClassName + ").ID(\"f_" + clm.DbColumnNameToClassName + "\").Label(\"" + showName + "\")" + showStr;
                    }
            }
        }
    }
}
