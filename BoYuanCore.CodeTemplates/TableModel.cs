﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoYuanCore.CoreCodeTemplates.DBMappingRules;


namespace BoYuanCore.CoreCodeTemplates
{
    public class TableModel
    {
        private string _dbClass=string.Empty;

        /// <summary>
        /// 数据，表的字段信息
        /// </summary>
        public List<DbColumnInfo> Columns { get; set; }

        /// <summary>
        /// 命名空间
        /// </summary>
        public string NamespaceStr { get; set; }

        /// <summary>
        /// 二级命名空间
        /// </summary>
        public string Namespace2Str { get; set; }

        /// <summary>
        /// 页面继承类名称
        /// </summary>
        public string ClassnameStr { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 表名对应的类名称(首字母大写)
        /// </summary>
        public string TableNameToClassName => DataTypeMapping.GetFormatName(TableName); //首字母大写

        /// <summary>
        /// 表备注信息
        /// </summary>
        public string TableDescription { get; set; }

        /// <summary>
        /// 实体命名空间名称
        /// </summary>
        public string ModelName { get; set; }

        /// <summary>
        /// 数据库类型  sqlserver mysql oracle PostgreSQL 等 (参考 FreeSql.DataType.SqlServer)
        /// </summary>
        public string DbClass
        {
            get => _dbClass.ToLower();
            set => _dbClass = value;
        }
    }
}
