﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeSql.DataAnnotations;

namespace BoYuanCore.DBD.Entity
{
    /// <summary>
    /// 雪花id实体
    /// </summary>
    public class SnowflakEntity
    {
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        [Column(IsPrimary = true)]
        public virtual long ID { get; set; } //= IdWorker.Instance.NextId();//默认雪花id赋值

        /// <summary>
        /// 给grid绑定用(由于前端js对long长字符串支持很差，所以改成string类型)
        /// </summary>
        [Column(IsIgnore = true)]
        public virtual string GridID => ID.ToString();
    }
}
