﻿using System;
using FreeSql.DataAnnotations;

namespace BoYuanCore.DBD.Entity
{
    ///<summary>
    ///后台管理员表
    ///</summary>
    [Table(Name = "SysAdmin")]
    [Serializable]
    public partial class SysAdmin : SnowflakEntity
    {
        public SysAdmin()
        {
            this.RoleID = Convert.ToInt64("0");
            this.Sort = Convert.ToInt32("1000");
            this.IsDel = false;
        }

        ///<summary>
        ///Desc:登录名称
        ///Default:
        ///Nullable:False
        ///</summary>
        [Column(StringLength = 25)]
        public string LoginName { get; set; }

        ///<summary>
        ///Desc:真实姓名
        ///Default:
        ///Nullable:True
        ///</summary>
        [Column(StringLength = 30)]
        public string RealName { get; set; }

        ///<summary>
        ///Desc:密码
        ///Default:
        ///Nullable:True
        ///</summary>
        [Column(StringLength = 32)]
        public string Password { get; set; }

        ///<summary>
        ///Desc:权限角色
        ///Default:0
        ///Nullable:True
        ///</summary>
        public long RoleID { get; set; }

        ///<summary>
        ///Desc:备注
        ///Default:
        ///Nullable:True
        ///</summary>
        [Column(StringLength = 500)]
        public string Remark { get; set; }

        ///<summary>
        ///Desc:照片
        ///Default:
        ///Nullable:True
        ///</summary>
        [Column(StringLength = 100)]
        public string Photo { get; set; }

        ///<summary>
        ///Desc:排序
        ///Default:1000
        ///Nullable:True
        ///</summary>
        public int Sort { get; set; }

        ///<summary>
        ///Desc:是否假删除
        ///Default:0
        ///Nullable:True
        ///</summary>
        public bool IsDel { get; set; }



    }
}

