﻿
using System;
using FreeSql.DataAnnotations;

namespace BoYuanCore.DBD.Entity
{
    ///<summary>
    ///登录日志表
    ///</summary>
    [Table(Name = "SysLoginLog")]
    [Serializable]
    public partial class SysLoginLog  : SnowflakEntity
    {
        public SysLoginLog()
        {
             this.Addtime =DateTime.Now;
             this.PwdShow =Convert.ToString("");
             this.Remark =Convert.ToString("");
             this.UserAgent =Convert.ToString("");
             this.UserId =Convert.ToInt64("0");

        }

        ///<summary>
        ///Desc:创建时间
        ///Default:DateTime.Now
        ///Nullable:False
        ///</summary>
        public DateTime Addtime { get; set; }

        ///<summary>
        ///Desc:ip地址
        ///Default:
        ///Nullable:False
        ///</summary>
        [Column(StringLength = 500)]
        public string IP { get; set; }

        ///<summary>
        ///Desc:登录方式(1=QQ,2=Weibo,3=Weixin)
        ///Default:
        ///Nullable:False
        ///</summary>
        public byte LoginChannel { get; set; }

        ///<summary>
        ///Desc:登录状态
        ///Default:
        ///Nullable:False
        ///</summary>
        public byte LoginState { get; set; }

        ///<summary>
        ///Desc:登录失败后输入的密码明文
        ///Default:
        ///Nullable:False
        ///</summary>
        [Column(StringLength = 100)]
        public string PwdShow { get; set; }

        ///<summary>
        ///Desc:备注
        ///Default:
        ///Nullable:False
        ///</summary>
        public string Remark { get; set; }

        ///<summary>
        ///Desc:原始客户代理信息
        ///Default:
        ///Nullable:False
        ///</summary>
        [Column(StringLength = 150)]
        public string UserAgent { get; set; }

        ///<summary>
        ///Desc:登录成功后关联的用户id
        ///Default:0
        ///Nullable:False
        ///</summary>
        public long UserId { get; set; }

        ///<summary>
        ///Desc:输入的用户名
        ///Default:
        ///Nullable:False
        ///</summary>
        [Column(StringLength = 100)]
        public string UserName { get; set; }



    }
}

