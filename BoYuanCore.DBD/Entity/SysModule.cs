﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeSql.DataAnnotations;

namespace BoYuanCore.DBD.Entity
{
    ///<summary>
    /// 页面组件
    ///</summary>
    [Table(Name = "SysModule")]
    [Serializable]
    public partial class SysModule
    {
        public SysModule()
        {
            this.ButtonID = Convert.ToString("");
            this.ButtonText = Convert.ToString("");
            this.FontIcon = Convert.ToString("");
            this.Icon = Convert.ToString("");
            this.IsHidden = false;
            this.IsLeaf = true;
            this.PageTitle = Convert.ToString("");
            this.Remark = Convert.ToString("");
            this.Sort = Convert.ToInt32("1000");
            this.TreePath = Convert.ToString("");
            this.Url = Convert.ToString("");

        }

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        [Column(IsPrimary = true)]
        public int ID { get; set; }//采用int，此表比较特殊(不建议并发插入数据)


        /// <summary>
        /// 给grid绑定用(由于前端js对long长字符串支持很差，所以改成string类型,为了兼容代码程序，这里不做修改)
        /// </summary>
        [Column(IsIgnore = true)]
        public string GridID
        {
            get { return ID.ToString(); }
        }


        ///<summary>
        ///Desc:button控件id
        ///Default:
        ///Nullable:True
        ///</summary>
        [Column(StringLength = 25)]
        public string ButtonID { get; set; }

        ///<summary>
        ///Desc:button控件文本
        ///Default:
        ///Nullable:True
        ///</summary>
        [Column(StringLength = 10)]
        public string ButtonText { get; set; }


        ///<summary>
        ///Desc:字体图标
        ///Default:
        ///Nullable:True
        ///</summary>
        [Column(StringLength = 15)]
        public string FontIcon { get; set; }

        ///<summary>
        ///Desc:图标
        ///Default:
        ///Nullable:True
        ///</summary>
        [Column(StringLength = 100)]
        public string Icon { get; set; }

        ///<summary>
        ///Desc:是否隐藏
        ///Default:0
        ///Nullable:False
        ///</summary>
        public bool IsHidden { get; set; }

        ///<summary>
        ///Desc:
        ///Default:1
        ///Nullable:False
        ///</summary>
        public bool IsLeaf { get; set; }

        ///<summary>
        ///Desc:页面名称
        ///Default:
        ///Nullable:True
        ///</summary>
        [Column(StringLength = 15)]
        public string PageTitle { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:False
        ///</summary>
        public int ParentID { get; set; }


        ///<summary>
        ///Desc:备注
        ///Default:
        ///Nullable:True
        ///</summary>
        [Column(StringLength = 75)]
        public string Remark { get; set; }

        ///<summary>
        ///Desc:排序
        ///Default:1000
        ///Nullable:False
        ///</summary>
        public int Sort { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [Column(StringLength = 127)]
        public string TreePath { get; set; }

        ///<summary>
        ///Desc:页面地址
        ///Default:
        ///Nullable:True
        ///</summary>
        [Column(StringLength = 100)]
        public string Url { get; set; }


    }
}
