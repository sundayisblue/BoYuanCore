﻿
using System;
using System.Linq;
using System.Text;
using FreeSql.DataAnnotations;

namespace BoYuanCore.DBD.Entity
{
    ///<summary>
    ///
    ///</summary>
    [Table(Name = "SysModulePermissions")]
    [Serializable]
    public partial class SysModulePermissions : SnowflakEntity
    {
        public SysModulePermissions()
        {

        }

        ///<summary>
        ///Desc:sysRole的id
        ///Default:
        ///Nullable:False
        ///</summary>
        public long RoleID { get; set; }

        ///<summary>
        ///Desc:SysModule的id
        ///Default:
        ///Nullable:False
        ///</summary>
        public long PageID { get; set; }



    }
}

