﻿
using System;
using System.Linq;
using System.Text;
using FreeSql.DataAnnotations;

namespace BoYuanCore.DBD.Entity
{
    ///<summary>
    ///
    ///</summary>
    [Table(Name = "SysRole")]
    [Serializable]
    public partial class SysRole : SnowflakEntity
    {
        public SysRole()
        {
             this.IsDel =false;
             Remark = string.Empty;
        }

        ///<summary>
        ///Desc:权限角色名称
        ///Default:
        ///Nullable:True
        ///</summary>
        [Column(StringLength = 30)]
        public string RoleName { get; set; }

        ///<summary>
        ///Desc:备注
        ///Default:""
        ///Nullable:false
        ///</summary>
        [Column(StringLength = 200)]
        public string Remark { get; set; }

        ///<summary>
        ///Desc:是否假删除
        ///Default:0
        ///Nullable:True
        ///</summary>
        public bool IsDel { get; set; }



    }
}

