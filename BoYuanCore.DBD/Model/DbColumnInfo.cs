﻿using System;
using System.Linq;
using System.Text;
using FreeSql;
using FreeSql.DataAnnotations;

namespace BoYuanCore.DBD.Model
{
    ///<summary>
    ///字段信息表
    ///</summary>
    [Table(Name="DbColumnInfo")]
    public partial class DbColumnInfo : IBaseClass
    {
        public DbColumnInfo()
        {


        }
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        [Column(IsPrimary = true, IsIdentity = true)]
        public int id { get; set; }

        /// <summary>
        /// Desc:表id
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int TableId { get; set; }

        /// <summary>
        /// Desc:工程id
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int objId { get; set; }

        /// <summary>
        /// Desc:字段名称
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string DbColumnName { get; set; }

        /// <summary>
        /// Desc:字段对应C#的类型
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string PropertyName { get; set; }

        /// <summary>
        /// Desc:数据库字段类型
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string DataType { get; set; }

        /// <summary>
        /// Desc:值长度(数字或者字符串,例如nvarchar(MAX))
        /// Default:
        /// Nullable:false
        /// </summary>           
        public string LengthValue { get; set; }

        /// <summary>
        /// Desc:默认值
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string DefaultValue { get; set; }

        /// <summary>
        /// Desc:字段备注信息
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string ColumnDescription { get; set; }

        /// <summary>
        /// Desc:是否可以为null
        /// Default:
        /// Nullable:False
        /// </summary>           
        public bool IsNullable { get; set; }

        /// <summary>
        /// Desc:是否自增长
        /// Default:
        /// Nullable:False
        /// </summary>           
        public bool IsIdentity { get; set; }

        /// <summary>
        /// Desc:是否为主键
        /// Default:
        /// Nullable:False
        /// </summary>           
        public bool IsPrimarykey { get; set; }

    }
}
