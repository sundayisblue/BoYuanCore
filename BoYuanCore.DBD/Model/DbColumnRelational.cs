﻿using System;
using System.Linq;
using System.Text;
using FreeSql;
using FreeSql.DataAnnotations;

namespace BoYuanCore.DBD.Model
{
    ///<summary>
    ///字段关系表
    ///</summary>
    [Table(Name="DbColumnRelational")]
    public partial class DbColumnRelational : IBaseClass
    {
        public DbColumnRelational()
        {


        }
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        [Column(IsPrimary = true, IsIdentity = true)]
        public int id { get; set; }

        /// <summary>
        /// Desc:工程id
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int objId { get; set; }

        /// <summary>
        /// Desc:目标表id
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int TableId1 { get; set; }

        /// <summary>
        /// Desc:目标表字段id
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int colId1 { get; set; }

        /// <summary>
        /// Desc:外联表id
        /// Default:
        /// Nullable:True
        /// </summary>           
        public int? TableId2 { get; set; }

        /// <summary>
        /// Desc:外联表字段id
        /// Default:
        /// Nullable:True
        /// </summary>           
        public int? colId2 { get; set; }

        /// <summary>
        /// Desc:关系类型
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int RelationalType { get; set; }

        /// <summary>
        /// Desc:标签内容1
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string taginfo1 { get; set; }

        /// <summary>
        /// Desc:标签内容2
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string taginfo2 { get; set; }

        /// <summary>
        /// Desc:标签内容3
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string taginfo3 { get; set; }

        /// <summary>
        /// Desc:标签内容4
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string taginfo4 { get; set; }

        /// <summary>
        /// Desc:标签内容5
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string taginfo5 { get; set; }

        /// <summary>
        /// Desc:标签内容6
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string taginfo6 { get; set; }

        /// <summary>
        /// Desc:标签内容7
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string taginfo7 { get; set; }

        /// <summary>
        /// Desc:标签内容8
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string taginfo8 { get; set; }

        /// <summary>
        /// Desc:标签内容9
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string taginfo9 { get; set; }

    }
}
