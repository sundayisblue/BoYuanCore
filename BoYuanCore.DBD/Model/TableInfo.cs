﻿using System;
using System.Linq;
using System.Text;
using FreeSql;
using FreeSql.DataAnnotations;

namespace BoYuanCore.DBD.Model
{
    ///<summary>
    ///表信息
    ///</summary>
    [Table(Name="TableInfo")]
    public partial class TableInfo : IBaseClass
    {
        public TableInfo()
        {


        }
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        [Column(IsPrimary = true, IsIdentity = true)]
        public int id { get; set; }

        /// <summary>
        /// Desc:工程id
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int objId { get; set; }

        /// <summary>
        /// Desc:表名称
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string TableName { get; set; }

        /// <summary>
        /// Desc:表备注信息
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string TableDescription { get; set; }

    }
}
