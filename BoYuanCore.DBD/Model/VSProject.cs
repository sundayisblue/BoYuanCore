﻿using System;
using System.Linq;
using System.Text;
using FreeSql;
using FreeSql.DataAnnotations;
namespace BoYuanCore.DBD.Model
{
    ///<summary>
    ///工程信息
    ///</summary>
    [Table(Name="VSProject")]
    public partial class VSProject : IBaseClass
    {
        public VSProject()
        {


        }
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        [Column(IsPrimary = true, IsIdentity = true)]
        public int id { get; set; }

        /// <summary>
        /// Desc:工程名称
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string ProjectName { get; set; }

        /// <summary>
        /// Desc:创建时间
        /// Default:
        /// Nullable:False
        /// </summary>           
        public DateTime addtime { get; set; }

        /// <summary>
        /// Desc:备注信息
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string remark { get; set; }

        /// <summary>
        /// Desc:数据库连接字符串
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string sqlstr { get; set; }

    }
}
