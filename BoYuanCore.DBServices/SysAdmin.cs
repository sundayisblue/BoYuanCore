﻿using System;
using System.Collections.Generic;
using System.Text;
using BoYuanCore.DBServices.Base;
using BoYuanCore.Framework.MemoryCache;

namespace BoYuanCore.DBServices
{
    public class SysAdmin
    {
        private const string cacheKey = nameof(Entities.SysAdmin);
      

        /// <summary>
        /// 获取缓存key
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private static string GetCacheKey(long id)
        {
            return cacheKey + "_" + id;
        }

        /// <summary>
        /// 从缓存中获取用户信息
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="_iCachingProvider"></param>
        /// <returns></returns>
        public static Entities.SysAdmin GetAdminByCache(long userid, ICachingProvider _iCachingProvider)
        {
            return _iCachingProvider.GetOrSetObjectFromCache(GetCacheKey(userid), () => FreeSqlHelper.GetModel<Entities.SysAdmin>(userid));
        }

        /// <summary>
        /// 更新缓存
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="_iCachingProvider"></param>
        public static void RemoveCache(long userid, ICachingProvider _iCachingProvider)
        {
            _iCachingProvider.Remove(GetCacheKey(userid));//更新缓存
        }
    }
}
