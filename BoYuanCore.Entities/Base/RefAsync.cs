﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoYuanCore.Entities.Base
{

    //参考sqlsugar  RefAsync

    /// <summary>
    /// 解决async 不能获取ref 参数值问题
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RefAsync<T>
    {
        public RefAsync() { }
        public RefAsync(T value) { Value = value; }
        public T Value { get; set; }
        public override string ToString()
        {
            T value = Value;
            return value == null ? "" : value.ToString();
        }
        public static implicit operator T(RefAsync<T> r) { return r.Value; }
        public static implicit operator RefAsync<T>(T value) { return new RefAsync<T>(value); }
    }
}
