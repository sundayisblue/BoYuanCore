﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoYuanCore.Entities.EnumClass
{
    public class SysLoginLog
    {
        /// <summary>
        /// 登录方式
        /// </summary>
        public enum LoginChannel
        {
            PC = 1,
            QQ = 2,
            Weibo = 3,
            WeChat = 4
        }

        /// <summary>
        /// 登录状态
        /// </summary>
        public enum LoginState
        {
            登录失败 = 0,
            登录成功 = 1,
            登录异常 = 20,
        }
    }
}
