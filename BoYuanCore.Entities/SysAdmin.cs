﻿using System;
using System.ComponentModel;
using FreeSql.DataAnnotations;
using Newtonsoft.Json;

namespace BoYuanCore.Entities
{
    ///<summary>
    ///后台管理员表
    ///</summary>
    [Table(Name = "SysAdmin")]
    [Serializable]
    [JsonObject(MemberSerialization.OptIn)]
    public partial class SysAdmin : SnowflakEntity
    {
        public SysAdmin()
        {
            this.RoleID = 0;
            this.Sort =1000;
            this.IsDel = false;
        }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:False
        ///</summary>
        [JsonProperty]
        public bool IsDel { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 25)]
        public string LoginName { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 32)]
        public string Password { get; set; }

        ///<summary>
        ///Desc:
        ///Default:('')
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 100)]
        public string Photo { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 30)]
        public string RealName { get; set; }

        ///<summary>
        ///Desc:
        ///Default:('')
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 500)]
        public string Remark { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:False
        ///</summary>
        [JsonProperty]
        public long RoleID { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:False
        ///</summary>
        [JsonProperty]
        public int Sort { get; set; }



    }
}

