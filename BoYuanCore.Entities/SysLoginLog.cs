﻿
using System;
using FreeSql.DataAnnotations;
using Newtonsoft.Json;

namespace BoYuanCore.Entities
{
    ///<summary>
    ///登录日志表
    ///</summary>
    [Table(Name = "SysLoginLog")]
    [Serializable]
    [JsonObject(MemberSerialization.OptIn)]
    public partial class SysLoginLog  : SnowflakEntity
    {
        public SysLoginLog()
        {
            this.Addtime =DateTime.Now;
            this.PwdShow =string.Empty;
            this.Remark =string.Empty;
            this.UserAgent =string.Empty;
            this.UserId =0;

        }

        ///<summary>
        ///Desc:
        ///Default:(getdate())
        ///Nullable:False
        ///</summary>
        [JsonProperty,Column(ServerTime = DefaultDateTimeKind)]
        public DateTime Addtime { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 500)]
        public string IP { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:False
        ///</summary>
        [JsonProperty]
        public EnumClass.SysLoginLog.LoginChannel LoginChannel { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:False
        ///</summary>
        [JsonProperty]
        public EnumClass.SysLoginLog.LoginState LoginState { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 100)]
        public string PwdShow { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 255)]
        public string Remark { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 150)]
        public string UserAgent { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:False
        ///</summary>
        [JsonProperty]
        public long UserId { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 100)]
        public string UserName { get; set; }



    }
}

