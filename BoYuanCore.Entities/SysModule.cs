
using System;
using System.Linq;
using System.Text;
using FreeSql.DataAnnotations;
using Newtonsoft.Json;

namespace BoYuanCore.Entities
{
    ///<summary>
    ///
    ///</summary>
    [Table(Name = "SysModule")]
    [Serializable]
    [JsonObject(MemberSerialization.OptIn)]
    public partial class SysModule 
    {
        public SysModule()
        {
            this.ButtonID =string.Empty;
            this.ButtonText =string.Empty;
            this.FontIcon =string.Empty;
            this.Icon =string.Empty;
            this.IsHidden = false;
            this.IsLeaf = true;
            this.PageTitle =string.Empty;
            this.Remark =string.Empty;
            this.Sort = 1000;
            this.TreePath =string.Empty;
            this.Url =string.Empty;

        }


        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        [Column(IsPrimary = true)]
        public int ID { get; set; }//采用int，此表比较特殊(不建议并发插入数据)


        /// <summary>
        /// 给grid绑定用(由于前端js对long长字符串支持很差，所以改成string类型,为了兼容代码程序，这里不做修改)
        /// </summary>
        [Column(IsIgnore = true)]
        public string GridID
        {
            get { return ID.ToString(); }
        }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 25)]
        public string ButtonID { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 10)]
        public string ButtonText { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 15)]
        public string FontIcon { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 100)]
        public string Icon { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:False
        ///</summary>
        [JsonProperty]
        public bool IsHidden { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:False
        ///</summary>
        [JsonProperty]
        public bool IsLeaf { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 15)]
        public string PageTitle { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:False
        ///</summary>
        [JsonProperty]
        public int ParentID { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 75)]
        public string Remark { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:False
        ///</summary>
        [JsonProperty]
        public int Sort { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 127)]
        public string TreePath { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 100)]
        public string Url { get; set; }


    }
}

