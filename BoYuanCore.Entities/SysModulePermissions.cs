﻿
using System;
using System.Linq;
using System.Text;
using FreeSql.DataAnnotations;
using Newtonsoft.Json;

namespace BoYuanCore.Entities
{
    ///<summary>
    ///
    ///</summary>
    [Table(Name = "SysModulePermissions")]
    [Serializable]
    [JsonObject(MemberSerialization.OptIn)]
    public partial class SysModulePermissions : SnowflakEntity
    {
        public SysModulePermissions()
        {

        }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:False
        ///</summary>
        [JsonProperty]
        public long PageID { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:False
        ///</summary>
        [JsonProperty]
        public long RoleID { get; set; }




    }
}

