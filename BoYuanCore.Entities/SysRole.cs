﻿
using System;
using System.Linq;
using System.Text;
using FreeSql.DataAnnotations;
using Newtonsoft.Json;

namespace BoYuanCore.Entities
{
    ///<summary>
    ///
    ///</summary>
    [Table(Name = "SysRole")]
    [Serializable]
    [JsonObject(MemberSerialization.OptIn)]
    public partial class SysRole : SnowflakEntity
    {
        public SysRole()
        {
             this.IsDel =false;
             Remark = string.Empty;
        }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:False
        ///</summary>
        [JsonProperty]
        public bool IsDel { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 200)]
        public string Remark { get; set; }

        ///<summary>
        ///Desc:
        ///Default:
        ///Nullable:True
        ///</summary>
        [JsonProperty, Column(StringLength = 30)]
        public string RoleName { get; set; }


    }
}

