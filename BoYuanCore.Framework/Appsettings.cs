﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BoYuanCore.Framework
{
    /// <summary>
    /// appsettings.json操作类(注意是bin下的)
    /// </summary>
    public class Appsettings
    {
        static IConfiguration Configuration { get; set; }

        /// <summary>
        /// 更新地址(注意路径，默认使用AppContext.BaseDirectory是bin目录下的)
        /// </summary>
        /// <param name="contentPath"></param>
        public Appsettings(string contentPath)
        {
            string Path = "appsettings.json";

            //如果你把配置文件 是 根据环境变量来分开了，可以这样写
            //Path = $"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json";

            Configuration = new ConfigurationBuilder()
                .SetBasePath(contentPath)
                .Add(new JsonConfigurationSource { Path = Path, Optional = false, ReloadOnChange = true })//这样的话，可以直接读目录里的json文件，而不是 bin 文件夹下的，所以不用修改复制属性
                .Build();

            /*
            关于路径的说明：
            Env.ContentRootPath         代表项目根目录下，发布后还是根目录。非静态 不适用于Program.cs里设置。
            AppContext.BaseDirectory    代表项目bin目录下，发布后还是根目录。静态 适用于Program.cs里设置。
             */

        }

        /// <summary>
        /// 封装要操作的字符
        /// </summary>
        /// <param name="sections">节点配置</param>
        /// <returns></returns>
        public static string app(params string[] sections)
        {
            try
            {
                if (sections.Any())
                {
                    return Configuration[string.Join(":", sections)];
                }
            }
            catch (Exception) { }

            return string.Empty;
        }

        //调用方式： private static string _ConnectionString => Appsettings.app(new string[] { "AppSettings", "DbConnection", "ConnectionString" });
    }
}
