﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace BoYuanCore.Framework
{
    /// <summary>
    /// 配置文件实体
    /// </summary>
    public class ConfigModel
    {
        public class LogLevel
        {

            [JsonProperty("Default")]
            public string Default { get; set; }

            [JsonProperty("Microsoft")]
            public string Microsoft { get; set; }

            [JsonProperty("Microsoft.Hosting.Lifetime")]
            public string MicrosoftHostingLifetime { get; set; }
        }

        public class Logging
        {

            [JsonProperty("LogLevel")]
            public LogLevel LogLevel { get; set; }
        }

        public class Override
        {

            [JsonProperty("Microsoft")]
            public string Microsoft { get; set; }

            [JsonProperty("System")]
            public string System { get; set; }
        }

        public class MinimumLevel
        {

            [JsonProperty("Default")]
            public string Default { get; set; }

            [JsonProperty("Override")]
            public Override Override { get; set; }
        }

        public class Serilog
        {

            [JsonProperty("MinimumLevel")]
            public MinimumLevel MinimumLevel { get; set; }
        }

        public class DbConnection
        {

            [JsonProperty("ConnectionString")]
            public string ConnectionString { get; set; }
        }

        public class Redis
        {

            [JsonProperty("ConnectionString")]
            public string ConnectionString { get; set; }
        }

        public class AppSettings
        {

            [JsonProperty("DbConnection")]
            public DbConnection DbConnection { get; set; }

            [JsonProperty("Redis")]
            public Redis Redis { get; set; }
        }

        public class SampleResponse1
        {

            [JsonProperty("AllowedHosts")]
            public string AllowedHosts { get; set; }

            [JsonProperty("AppSettings")]
            public AppSettings AppSettings { get; set; }

            [JsonProperty("Logging")]
            public Logging Logging { get; set; }

            [JsonProperty("Serilog")]
            public Serilog Serilog { get; set; }
        }
    }
}
