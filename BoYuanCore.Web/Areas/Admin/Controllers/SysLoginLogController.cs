﻿
using FineUICore;

using System;
using System.Linq;
using BoYuanCore.DBServices.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BoYuanCore.Web.Areas.Admin.Controllers
{
   
    public partial class SysLoginLogController : BaseController
    {
        private readonly ILogger<SysLoginLogController> _logger;
        public SysLoginLogController(ILogger<SysLoginLogController> logger) : base(logger)
        {
            _logger = logger;
        }

        #region ListPage
        // GET: Admin/SysLoginLog
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ShowGrid(IFormCollection fc)
        {
            var keyword = fc["txb_keyword"].ToString().Trim();
            var queryable = DB.Queryable<BoYuanCore.Entities.SysLoginLog>()
                .WhereIf(keyword.Length > 0, p=>p.UserName.Contains(keyword))
                .WhereIf(DateTime.TryParse(fc["dp_Date1"].ToString(),out DateTime dt1Result), p=>p.Addtime>= dt1Result)
                .WhereIf(DateTime.TryParse(fc["dp_Date2"].ToString(),out DateTime dt2Result), p=>p.Addtime<= dt2Result)
                .OrderByDescending(p => p.ID)
                ;

            var grid1 = UIHelper.Grid("Grid1");
            grid1.BindGrid(fc, "ddlPageSize", queryable);
            return UIHelper.Result();
        }

        
        #endregion

    }
}

