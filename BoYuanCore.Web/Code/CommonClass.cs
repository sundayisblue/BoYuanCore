﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;

namespace BoYuanCore.WebUI.Code
{
    /// <summary>
    /// 公用方法
    /// </summary>
    public class CommonClass
    {

        /// <summary>
        /// 获取项目中的路径(根据view位置)
        /// </summary>
        /// <param name="env"></param>
        /// <returns></returns>
        public static List<string> GetMvcUrl(IHostingEnvironment env)
        {
            DirectoryInfo adminDir = new DirectoryInfo(env.ContentRootPath + "/Areas/"+UrlAddress.AreaAdmin+"/Views");

            //获取文件夹，排除home文件夹
            var dirList = adminDir.GetDirectories().Where(p=>p.Name!= "Home" && p.Name!="Login").OrderBy(p => p.Name);

            IEnumerable<FileInfo> cshtmlList; 
            List<string> urlList =new List<string>();
            foreach (var dir in dirList)
            {
                cshtmlList = dir.EnumerateFiles("*.cshtml").OrderBy(p => p.Name);
                
                foreach (var cshtml in cshtmlList)
                {
                    urlList.Add("/" + UrlAddress.AreaAdmin + "/"+dir.Name + "/" + cshtml.Name.Replace(".cshtml",string.Empty));
                }
            }

            return urlList;
        }

    }
}
