﻿using System;
using BoYuanCore.DBServices.Base;
using BoYuanCore.WebUI.Code.Filter;
using FineUICore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using BoYuanCore.Framework.MemoryCache;
using BoYuanCore.WebUI.Code;
using Microsoft.AspNetCore.Mvc;

namespace BoYuanCore.Web
{
    [Area(UrlAddress.AreaAdmin)]
    [Authorize]
    [AuthorizationFilter] //授权特性
     //自定义权限许可
    public class BaseController : FineUICoreExtensions.FineUICoreController
    {
        /// <summary>
        /// 项目是否包含button权限
        /// </summary>
        public const bool HaveButtonPermissions = false;//如果包含请设置为true

        private ILogger<BaseController> _logger;

        /// <summary>
        /// 全局DB访问对象,简化写法,可以直接支持事务。
        /// </summary>
        public static IFreeSql DB = FreeSqlHelper.Fsql;
        public BaseController(ILogger<BaseController> logger)//子类调用
        {
            _logger = logger;
        }

        /// <summary>
        /// 获取当前用户id
        /// </summary>
        /// <returns></returns>
        protected long GetUserID()
        {
            ClaimsPrincipal principal = ControllerContext.HttpContext.User;
            if (null != principal)
            {
                foreach (Claim claim in principal.Claims)
                {
                    if (claim.Type == "userid")
                    {
                        return Convert.ToInt64(claim.Value);
                    }
                }
            }
            return 0;
        }

        /// <summary>
        /// 动态添加页面初始化js代码
        /// </summary>
        /// <param name="js"></param>
        public void AddInitLoadJs(string js)
        {
            string temp = js.Trim();
            if(string.IsNullOrEmpty(temp))return;
            if (temp.Substring(temp.Length - 1) != ";") js += ";";//判断js最后是否有分号

            ViewBag.InitLoadJs += (js+System.Environment.NewLine);
        }
    }
}