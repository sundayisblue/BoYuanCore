﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoYuanCore.Framework.Extensions;
using BoYuanCore.WebUI.Code;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace BoYuanCore.WebUI.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult Index()
        {
            //Startup,Configure 添加 app.UseExceptionHandler(UrlAddress.ErrorUrl);//全局异常拦截 //参考ErrorController

            var feature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            if (feature != null)
            {
                var exception = feature.Error;

                string num = Guid.NewGuid().ToString("N");//错误码

                ViewBag.errorNum = num;
                string msg = num + " >> "
                                 + exception.Message.LineBreak() 
                                 + "========StackTrace=======".LineBreak()
                                 + exception.StackTrace;
                Log.Error(msg);
            }
            return View();
        }

        [Route("Error/{statusCode}")]
        public IActionResult CustomError(int statusCode)
        {
            //https://www.cnblogs.com/dudu/p/6004777.html

            if (statusCode == 404)
            {
                return View("~/Views/Error/404.cshtml");
            }
            return View("~/Views/Error/500.cshtml");
        }

        public IActionResult NoAccess()
        {
            return View();
        }
    }
}