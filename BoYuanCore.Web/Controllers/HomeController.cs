﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using BoYuanCore.WebUI.Code;
using FineUICore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;

namespace BoYuanCore.Web
{
    
    [Route("[controller]")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger) 
        {
            _logger = logger;
        }

        // GET: Home
        [Route("")]
        [Route("/")]
        public IActionResult Index()
        {
            //根目录首页 暂时搁置
            return RedirectToAction("Index", "Login", new { area = UrlAddress.AreaAdmin });
        }


    }
}