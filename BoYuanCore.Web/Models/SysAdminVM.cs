﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FreeSql.DataAnnotations;

namespace BoYuanCore.Web.Models
{
    public class SysAdminVM
    {
        public long ID { get; set; }

        ///<summary>
        ///Desc:登录名称
        /// </summary>
        public string LoginName { get; set; }

        ///<summary>
        ///Desc:真实姓名
        /// </summary>
        public string RealName { get; set; }


        ///<summary>
        ///Desc:权限角色
        /// </summary>
        public long RoleID { get; set; }

        ///<summary>
        ///Desc:备注
        /// </summary>
        public string Remark { get; set; }

        ///<summary>
        ///Desc:照片
        /// </summary>
        public string Photo { get; set; }

        ///<summary>
        ///Desc:排序
        /// </summary>
        public int Sort { get; set; }

        ///<summary>
        ///Desc:是否假删除
        /// </summary>
        public bool IsDel { get; set; }

        //------扩展------
        [Column(IsIgnore = true)]
        public string GridID => ID.ToString();

        /// <summary>
        /// 权限名称
        /// </summary>
        [Column(IsIgnore = true)] 
        public string RoleName { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(IsIgnore = true)]
        public bool State => !IsDel;

    }

}
