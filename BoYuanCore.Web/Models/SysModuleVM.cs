﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FineUICore;
using FreeSql.DataAnnotations;
using FreeSql.Internal.Model;

namespace BoYuanCore.WebUI.Models
{
    [Table(Name = "SysModule")]
    public class SysModuleVM:Entities.SysModule
    {
        /// <summary>
        /// 显示字体图标
        /// </summary>
        [Column(IsIgnore = true)]
        public string ShowFontIcon
        {
            get
            {
                if (string.IsNullOrEmpty(FontIcon) || FontIcon == "None") return string.Empty;

                IconFont iconType = (IconFont)Enum.Parse(typeof(IconFont), FontIcon);
                string iconName = IconFontHelper.GetName(iconType);

                if (FontIcon.StartsWith("_")) // 以下划线开头的是IconFont字体
                {
                    return string.Format("<li class=\"f-icon f-iconfont {0}\"></li>", iconName);
                }
                else
                {
                    return string.Format("<li class=\"f-icon f-icon-{0}\"></li>", iconName);
                }
            }
        }

        /// <summary>
        /// 显示button相关信息
        /// </summary>
        [Column(IsIgnore = true)]
        public string ShowButtonInfo { get; set; }
    }
}
