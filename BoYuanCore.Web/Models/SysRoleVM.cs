﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FreeSql.DataAnnotations;

namespace BoYuanCore.Web.Models
{
    [Table(Name = "SysRole")]
    public class SysRoleVM:Entities.SysRole
    {
        /// <summary>
        /// 状态
        /// </summary>
        [Column(IsIgnore = true)] 
        public bool State => !IsDel;

    }
}
