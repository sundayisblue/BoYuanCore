using System;
using System.IO;
using System.Reflection;
using BoYuanCore.Framework;
using BoYuanCore.Framework.MemoryCache;
using BoYuanCore.Web.Code.Filter;
using BoYuanCore.WebUI.Code;
using BoYuanCore.WebUI.Code.Extensions;
using FineUICore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;

namespace BoYuanCore.Web
{
    public class Startup
    {
        private readonly IConfiguration Configuration;
        private readonly IWebHostEnvironment Env;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();//http对象相关
            services.AddSingleton(new Appsettings(Path.Combine(AppContext.BaseDirectory, "configs")));//注意使用的bin目录下configs文件夹

            services.AddMemoryCache();//MemoryCache
            services.AddDistributedMemoryCache();//分布式内存缓存
            services.AddSingleton(typeof(ICachingProvider), typeof(MemoryCaching));

            //services.AddScoped<CustomCookieAuthenticationEvents>();//cookie认证事件注入
            services.AddSession(option =>
            {
                option.IdleTimeout = TimeSpan.FromHours(1); //过期时间
                option.Cookie.HttpOnly = true;
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options =>
            {
                //options.EventsType = typeof(CustomCookieAuthenticationEvents);// cookie认证事件拦截
                options.LoginPath = new PathString(UrlAddress.LoginUrl);//身份验证失败，跳转到指定位置
                options.Cookie.HttpOnly = true;
            });

            // 配置请求参数限制
            services.Configure<FormOptions>(x =>
            {
                x.ValueCountLimit = 1024;   // 请求参数的个数限制（默认值：1024）
                x.ValueLengthLimit = 4194304;   // 单个请求参数值的长度限制（默认值：4194304 = 1024 * 1024 * 4）
            });


            //services.AddFreeSqlSetup();//暂时不用此方式。通过DBServices类库注册FreeSql 

          
            services.AddFineUI(Configuration);// FineUI 服务

            services.AddControllersWithViews()
                .AddMvcOptions(options =>
            {
                // 自定义模型绑定（Newtonsoft.Json）
                options.ModelBinderProviders.Insert(0, new JsonModelBinderProvider());
            }).AddRazorRuntimeCompilation()
                .AddNewtonsoftJson();

            //services.AddControllersWithViews(a => { a.Filters.Add(typeof(ExceptionFilter)); });//全局异常拦截（弃用此方法，参看error页面）

            //注入文件管理
            var physicalProvider = Env.ContentRootFileProvider;
            var embeddedProvider = new EmbeddedFileProvider(Assembly.GetEntryAssembly());
            var compositeProvider = new CompositeFileProvider(physicalProvider, embeddedProvider);
            // choose one provider to use for the app and register it
            services.AddSingleton<IFileProvider>(physicalProvider);
            services.AddSingleton<IFileProvider>(embeddedProvider);
            services.AddSingleton<IFileProvider>(compositeProvider);

            FreeSqlSetup.FreeSqlAop();//注册FreeSql Aop
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())//是否为开发模式
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseExceptionHandler(UrlAddress.ErrorUrl);
                //app.UseStatusCodePagesWithReExecute(UrlAddress.Page404Url, null);
            }

            app.UseExceptionHandler(UrlAddress.ErrorUrl);//全局异常拦截
            app.UseStatusCodePagesWithReExecute(UrlAddress.Page404Url);//404页面处理

            // 推荐使用特性路由，路由参考： https://docs.microsoft.com/zh-cn/aspnet/core/mvc/controllers/routing?view=aspnetcore-3.1
            //DefaultFilesOptions options = new DefaultFilesOptions();
            //options.DefaultFileNames.Clear();
            //options.DefaultFileNames.Add("/home/index.html");    //将index.html改为需要默认起始页的文件名.
            //app.UseDefaultFiles(options);

            app.UseStaticFiles();//静态文件

            //app.UseSerilogRequestLogging(opts => opts.EnrichDiagnosticContext = LogHelper.EnrichFromRequest);//Serilog中间件 https://www.cnblogs.com/yilezhu/p/12227271.html

            app.UseSession();

            app.UseRouting();//使用路由方式(mvc,RazorPage,signalr等)

            app.UseAuthentication();//添加认证
            app.UseAuthorization();//使用身份授权 Authorize特性

            app.UseFineUI(); // FineUI 中间件（确保 UseFineUI 位于 UseEndpoints 的前面）

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: UrlAddress.AreaAdmin,
                    pattern: "{area:exists}/{controller=Login}/{action=Index}/{id?}");


                // endpoints.MapAreaControllerRoute(
                //     name: "area",
                //     areaName: UrlAddress.AreaAdmin,
                //     pattern: UrlAddress.AreaAdmin+"/{controller=Login}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=HomePage}/{action=Index}/{id?}");
            });
        }




    }
}
