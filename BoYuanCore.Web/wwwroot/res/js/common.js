﻿// Notify 显示居中通知对话框  js参数赋值 function(){alert('123')} ----------------------------------------------
function NotifyInfor(message, title, messageIcon) {
    F.notify({ target: '_top', message: message, messageIcon: messageIcon ? messageIcon : 'information',  header: false, displayMilliseconds: 3000, positionX: 'center', positionY: 'top' });
}

function NotifyError(message, title) {
    NotifyInfor(message, title, 'error');
}

function NotifySuccess(message, title) {
    NotifyInfor(message, title, 'success');
}

function NotifyQuestion(message, title) {
    NotifyInfor(message, title, 'question');
}

function NotifyWarning(message, title) {
    NotifyInfor(message, title, 'warning');
}

//QQ风格 Notify ---------------------------------------------------------------------------
function QQNotify(message, title, messageIcon) {
    F.notify({ target: '_top', message: message, messageIcon: messageIcon ? messageIcon : 'information', header: true, displayMilliseconds: 0, positionX: 'right', positionY: 'bottom', closable:true });
}

function QQNotifyError(message, title) {
    QQNotify(message, title, 'error');
}

function QQNotifySuccess(message, title) {
    QQNotify(message, title, 'success');
}

function QQNotifyQuestion(message, title) {
    QQNotify(message, title, 'question');
}

function QQNotifyWarning(message, title) {
    QQNotify(message, title, 'warning');
}

// Modal(组态风格) Notify ------------------------------------------------------------------
function ModalNotify(message, title, messageIcon) {
    F.notify({ target: '_top', message: message, messageIcon: messageIcon ? messageIcon : 'information', header: true, displayMilliseconds: 0, positionX: 'center', positionY: 'center', closable: true, modal:true });
}

function ModalNotifyError(message, title) {
    ModalNotify(message, title, 'error');
}

function ModalNotifySuccess(message, title) {
    ModalNotify(message, title, 'success');
}

function ModalNotifyQuestion(message, title) {
    ModalNotify(message, title, 'question');
}

function ModalNotifyWarning(message, title) {
    ModalNotify(message, title, 'warning');
}

//alert   js参数赋值 function(){alert('123')} ----------------------------------------------
function AlertInfor(message, title, js,messageIcon) {
    F.alert({ message: message, target: '_parent', title: title, messageIcon: messageIcon ? messageIcon : 'information', closable: true, ok:js  });
}

function AlertError(message, title, js) {
    AlertInfor(message, title, js, 'error');
}

function AlertSuccess(message, title,js) {
    AlertInfor(message, title, js, 'success');
}

function AlertQuestion(message, title,js) {
    AlertInfor(message, title, js, 'question');
}

function AlertWarning(message, title,js) {
    AlertInfor(message, title, js, 'warning');
}

//html 相关
function GetHtmlA(href, fileName, target, title) {
    if (!href || !fileName) return "";
    if (!target) target = '';
    if (!title) title = '';
    return F.formatString("<a href='{0}'{2}>{1}</a>", href, fileName, title + target)
}

function GetHtmlAJs(js, fileName) {
    if (!fileName) return '';
    if (!js) return fileName;
    return F.formatString("<a href='javascript:;' onclick=\"{0}\">{1}</a>", href, fileName, title + target)
}

function GetHtmlImg(src, title, alt, attr) {
    if (!src) return '';
    if (title && title.length > 0) { title = " title='" + title + "'"; }else { title = " "; }
    if (alt && alt.length > 0) { alt = " alt='" + alt + "'"; }else { alt = " "; }
    if (!attr) { attr = ''; }
    return F.formatString("<img src='{0}'{1}{2} {3}/>", src, title, alt, attr)
}

// grid 相关
function renderActions(value, params) {//取值示例 value,取其他值 params.rowValue.PageTitle ;//'<a class="action editit" href="javascript:;"><img class="f-grid-imagefield" src="' + F.baseUrl + 'res/icon/pencil.png"></a>'
    return renderBase('action editit', GetHtmlImg(F.baseUrl + 'res/icon/pencil.png', '', '','class="f-grid-imagefield"'));
}

function renderBase(className,info) {
    return '<a class="' + className+'" href="javascript:;">'+info+'</a>';
}
