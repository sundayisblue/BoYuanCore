﻿namespace BoYuanCore.WebForm.Code
{
    /// <summary>
    /// 静态网页地址(为有些地方不能使用Url.Action)
    /// </summary>
    public class UrlAddress
    {
        /// <summary>
        /// admin域名称
        /// </summary>
        public const string AreaAdmin = "Admin";

        /// <summary>
        /// 登录地址
        /// </summary>
        public const string LoginUrl = "/Admin/Login";

        /// <summary>
        /// 登录地址
        /// </summary>
        public const string HomeUrl = "/Admin/Home";

        /// <summary>
        /// 错误页面地址
        /// </summary>
        public const string ErrorUrl = "/Error";
        /// <summary>
        /// 错误页面地址
        /// </summary>
        public const string Page404Url = "/Error/{0}";

        /// <summary>
        /// 没有权限访问提示页面
        /// </summary>
        public const string NoAccess = "/Error/NoAccess";
    }
}
