﻿using FineUICore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Web;
using System.Xml;
using BoYuanCore.DBServices;
using BoYuanCore.WebForm.Code;
using BoYuanCore.Framework.MemoryCache;

namespace BoYuanCore.WebForm.Pages.Admin
{
    public partial class IndexModel : BaseModel
    {
        private readonly ICachingProvider _iCachingProvider;

        public IndexModel(ICachingProvider iCachingProvider)
        {
            _iCachingProvider = iCachingProvider;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        #region LoadData


        private string _displayMode = "normal";
        private string _mainTabs = "multi";

        private void LoadData()
        {
            string cookie = String.Empty;

            // 从Cookie中读取 - 显示模式
            cookie = Request.Cookies["DisplayMode"];
            if (!String.IsNullOrEmpty(cookie))
            {
                _displayMode = cookie;
            }

            // 从Cookie中读取 - 主选项卡标签
            cookie = Request.Cookies["MainTabs"];
            if (!String.IsNullOrEmpty(cookie))
            {
                _mainTabs = cookie;
            }


            // 初始化设置 - 显示模式
            SetCheckedMenuItem(MenuDisplayMode, _displayMode);

            // 初始化设置 - 主选项卡标签
            SetCheckedMenuItem(MenuMainTabs, _mainTabs);

            // v8.0 - 对图片背景的主题做特殊处理
            var customTheme = PageManagerExtension.Instance.Source.CustomTheme;
            if (customTheme == "custom_default" || customTheme.StartsWith("image_"))
            {
                sidebarRegion.CssClass += " bgpanel";
                topPanel.CssClass += " bgpanel";
            }

            mainTabStrip.ShowTabHeader = _mainTabs == "multi";

            LoadTreeMenuData();
        }

        private void LoadTreeMenuData()
        {
            var moduleList = DBServices.SysModule.GetSysModuleListByCache(_iCachingProvider, HaveButtonPermissions);
            long userid = GetUserID();
            List<long> moduleIds = DBServices.SysModulePermissions.GetPermissionsByCache(userid, _iCachingProvider);//获取当前用户的对应角色的页面组件id集合
            var list = moduleList.Where(p => moduleIds.Contains(p.ID) && string.IsNullOrEmpty(p.ButtonID) && p.IsHidden == false).OrderBy(p => p.Sort).ToList();
            CreateTreeNodes(list, 0, null);
        }

        private void CreateTreeNodes(List<Entities.SysModule> list, int pid, TreeNode node)
        {
            var parentlist = list.Where(p => p.ParentID == pid).OrderBy(p => p.Sort).ToList();

            TreeNode sonNode;
            foreach (var module in parentlist)
            {
                sonNode = new TreeNode();

                sonNode.NodeID = module.ID.ToString();
                sonNode.Text = module.PageTitle;
                sonNode.NavigateUrl = module.Url;

                if (!string.IsNullOrEmpty(module.Icon)) sonNode.IconUrl = "/" + module.Icon;
                if (!string.IsNullOrEmpty(module.FontIcon)) sonNode.IconFont = (IconFont)Enum.Parse(typeof(IconFont), module.FontIcon);

                sonNode.Expanded = true;//默认节点都为展开

                CreateTreeNodes(list, module.ID, sonNode);

                if (pid != 0)
                {
                    node.Nodes.Add(sonNode);
                }
                else
                {
                    treeMenu.Nodes.Add(sonNode);
                }
            }
        }

        #endregion

        #region SetCheckedMenuItem

        private void SetCheckedMenuItem(MenuButton menuButton, string checkedValue)
        {
            foreach (MenuItem item in menuButton.Menu.Items)
            {
                MenuCheckBox checkBox = (item as MenuCheckBox);
                if (checkBox != null)
                {
                    checkBox.Checked = checkBox.AttributeDataTag == checkedValue;
                }
            }
        }

        #endregion
    }
}