﻿//------------------------------------------------------------------------------
//     这段代码是由没想好生成的。
//     对这个文件的更改可能导致不正确的行为，并且如果代码重新生成，这些更改会丢失.
//     在 .cshtml 文件中添加 //NoFineUIDesigner 注释，以防止生成此文件。
//     或者在 appsettings 中添加 FineUIDesigner:false，以防止生成此文件。
//------------------------------------------------------------------------------
namespace BoYuanCore.WebForm.Pages.Admin
{
   public partial class IndexModel
   {
       /// <summary>
       /// Panel
       /// </summary>
       protected FineUICore.Panel mainPanel;
      
       /// <summary>
       /// Panel
       /// </summary>
       protected FineUICore.Panel sidebarRegion;
      
       /// <summary>
       /// Panel
       /// </summary>
       protected FineUICore.Panel sideheaderTopPanel;
      
       /// <summary>
       /// Panel
       /// </summary>
       protected FineUICore.Panel leftPanel;
      
       /// <summary>
       /// Tree
       /// </summary>
       protected FineUICore.Tree treeMenu;
      
       /// <summary>
       /// Panel
       /// </summary>
       protected FineUICore.Panel bodyRegion;
      
       /// <summary>
       /// Panel
       /// </summary>
       protected FineUICore.Panel topPanel;
      
       /// <summary>
       /// Button
       /// </summary>
       protected FineUICore.Button btnCollapseSidebar;
      
       /// <summary>
       /// Button
       /// </summary>
       protected FineUICore.Button btnThemeSelect;
      
       /// <summary>
       /// MenuButton
       /// </summary>
       protected FineUICore.MenuButton MenuDisplayMode;
      
       /// <summary>
       /// MenuCheckBox
       /// </summary>
       protected FineUICore.MenuCheckBox MenuDisplayModeCompact;
      
       /// <summary>
       /// MenuCheckBox
       /// </summary>
       protected FineUICore.MenuCheckBox MenuDisplayModeSmall;
      
       /// <summary>
       /// MenuCheckBox
       /// </summary>
       protected FineUICore.MenuCheckBox MenuDisplayModeNormal;
      
       /// <summary>
       /// MenuCheckBox
       /// </summary>
       protected FineUICore.MenuCheckBox MenuDisplayModeLarge;
      
       /// <summary>
       /// MenuCheckBox
       /// </summary>
       protected FineUICore.MenuCheckBox MenuDisplayModeLargeSpace;
      
       /// <summary>
       /// MenuButton
       /// </summary>
       protected FineUICore.MenuButton MenuMainTabs;
      
       /// <summary>
       /// MenuCheckBox
       /// </summary>
       protected FineUICore.MenuCheckBox MenuMainTabsMulti;
      
       /// <summary>
       /// MenuCheckBox
       /// </summary>
       protected FineUICore.MenuCheckBox MenuMainTabsSingle;
      
       /// <summary>
       /// TabStrip
       /// </summary>
       protected FineUICore.TabStrip mainTabStrip;
      
       /// <summary>
       /// Tab
       /// </summary>
       protected FineUICore.Tab tabHomepage;
      
       /// <summary>
       /// Tool
       /// </summary>
       protected FineUICore.Tool toolRefresh;
      
       /// <summary>
       /// Window
       /// </summary>
       protected FineUICore.Window windowThemeRoller;
      
       /// <summary>
       /// Window
       /// </summary>
       protected FineUICore.Window windowLoadingSelector;
      
   }
}
