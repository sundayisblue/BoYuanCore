﻿using BoYuanCore.DBServices.Base;
using BoYuanCore.WebForm.Code;
using FineUICore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BoYuanCore.WebForm.Pages.Admin.SysRole
{
    public partial class IndexModel : BaseModel
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Button_add.OnClientClick = Window1.GetShowReference("./Add", "添加角色");
                bind();
            }
        }

        private void bind()
        {
            bool isdelValue;
            if (cb_del.Checked)
            {
                Button_delete.Text = "批量还原";
                Button_delete.Icon = Icon.ArrowUp;
                isdelValue = true;
            }
            else
            {
                Button_delete.Text = "批量删除";
                Button_delete.Icon = Icon.Delete;
                isdelValue = false;

            }

            var list = DB.Select<Entities.SysRole>().Where(p => p.IsDel == isdelValue).OrderBy(p => p.ID).ToList();

            Grid1.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            Grid1.DataSource = list;
            Grid1.DataBind();
        }


       
    }
}