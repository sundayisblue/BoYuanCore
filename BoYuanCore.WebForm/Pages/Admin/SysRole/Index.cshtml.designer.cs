﻿//------------------------------------------------------------------------------
//     这段代码是由没想好生成的。
//     对这个文件的更改可能导致不正确的行为，并且如果代码重新生成，这些更改会丢失.
//     在 .cshtml 文件中添加 //NoFineUIDesigner 注释，以防止生成此文件。
//     或者在 appsettings 中添加 FineUIDesigner:false，以防止生成此文件。
//------------------------------------------------------------------------------
namespace BoYuanCore.WebForm.Pages.Admin.SysRole
{
   public partial class IndexModel
   {
       /// <summary>
       /// Panel
       /// </summary>
       protected FineUICore.Panel Panel1;
      
       /// <summary>
       /// Grid
       /// </summary>
       protected FineUICore.Grid Grid1;
      
       /// <summary>
       /// Button
       /// </summary>
       protected FineUICore.Button Button_add;
      
       /// <summary>
       /// Button
       /// </summary>
       protected FineUICore.Button Button_delete;
      
       /// <summary>
       /// CheckBox
       /// </summary>
       protected FineUICore.CheckBox cb_del;
      
       /// <summary>
       /// DropDownList
       /// </summary>
       protected FineUICore.DropDownList ddlPageSize;
      
       /// <summary>
       /// Window
       /// </summary>
       protected FineUICore.Window Window1;
      
   }
}
