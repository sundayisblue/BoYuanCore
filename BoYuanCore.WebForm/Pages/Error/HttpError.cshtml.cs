﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BoYuanCore.WebForm.Pages.Error
{
    public partial class HttpErrorModel : PageModel
    {
        public string Message { get; set; }
        public string Code {get; set; }

        [Route("Error/{statusCode:int}")] 
        public void OnGet(int statusCode)
        {
            if (statusCode == 404)
            {
                Code = "404";
                Message = "抱歉页面找不到了!";
            }
            else
            {
                Code = "500";
                Message = "服务器异常";
            }
        }
    }
}