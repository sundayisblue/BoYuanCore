﻿using BoYuanCore.Framework.Extensions;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Serilog;

namespace BoYuanCore.WebForm.Pages.Error
{
    public partial class IndexModel : PageModel
    {
        public string ErrorNum { get; set; }

        public void OnGet()
        {  
            //Startup,Configure 添加 app.UseExceptionHandler(UrlAddress.ErrorUrl);//全局异常拦截 //参考ErrorController

            var feature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            if (feature != null)
            {
                var exception = feature.Error;

                string num = Guid.NewGuid().ToString("N");//错误码

                ErrorNum = num;
                string msg = num + " >> "
                                 + exception.Message.LineBreak()
                                 + "========StackTrace=======".LineBreak()
                                 + exception.StackTrace;
                Log.Error(msg);
            }
        }

    }
}