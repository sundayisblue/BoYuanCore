﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace FineUICore
{
    public partial class FineUICoreExtensions
    {
        public class FineUICoreController : Controller
        {
            #region Alert 或 页面跳转

            /// <summary>
            /// 提示信息(弹层用),并指定父页面跳转
            /// </summary>
            /// <param name="message">内容</param>
            /// <param name="url">跳转地址</param>
            /// <param name="js">显示消息后 执行的js</param>
            protected void AlertInforAndRedirect(string message, string url, string js = "")
            {
                FineUICore.Alert aa = new FineUICore.Alert();
                aa.AlertInforAndRedirect(message, url, js);
            }

            /// <summary>
            /// 提示信息(弹层用)
            /// </summary>
            /// <param name="message">内容</param>
            /// <param name="isPostBackReference">是否回发刷新页面</param>
            /// <param name="js">显示消息后 执行的js</param>
            protected void AlertInfor(string message, bool isPostBackReference = true, string js = "")
            {
                FineUICore.Alert aa = new FineUICore.Alert();
                aa.AlertInfor(message, isPostBackReference, js);
            }

            /// <summary>
            /// 提示信息（单页用 并刷新当前页）
            /// </summary>
            /// <param name="message">内容</param>
            /// <param name="isPostBackReference">是否回发刷新页面</param>
            /// <param name="js">显示消息后 执行的js</param>
            protected void AlertInforBySingerPage(string message, bool isPostBackReference = true, string js = "")
            {
                FineUICore.Alert aa = new FineUICore.Alert();
                aa.AlertInforBySingerPage(message, isPostBackReference, js);

            }

            /// <summary>
            /// 报错信息
            /// </summary>
            /// <param name="message">内容</param>
            /// <param name="isPostBackReference">是否回发刷新页面</param>
            /// <param name="js">显示消息后 执行的js</param>
            protected void AlertError(string message, bool isPostBackReference = true, string js = "")
            {
                FineUICore.Alert aa = new FineUICore.Alert();
                aa.AlertError(message, isPostBackReference, js);
            }

            /// <summary>
            /// 成功信息
            /// </summary>
            /// <param name="message">内容</param>
            /// <param name="isPostBackReference">是否回发刷新页面</param>
            /// <param name="js">显示消息后 执行的js</param>
            protected void AlertSuccess(string message, bool isPostBackReference = true, string js = "")
            {
                FineUICore.Alert aa = new FineUICore.Alert();
                aa.AlertSuccess(message, isPostBackReference, js);
            }

            /// <summary>
            /// 疑问信息
            /// </summary>
            /// <param name="message">内容</param>
            /// <param name="isPostBackReference">是否回发刷新页面</param>
            /// <param name="js">显示消息后 执行的js</param>
            protected void AlertQuestion(string message, bool isPostBackReference = true, string js = "")
            {
                FineUICore.Alert aa = new FineUICore.Alert();
                aa.AlertQuestion(message, isPostBackReference, js);
            }

            /// <summary>
            /// 警告信息
            /// </summary>
            /// <param name="message">内容</param>
            /// <param name="isPostBackReference">是否回发刷新页面</param>
            /// <param name="js">显示消息后 执行的js</param>
            protected void AlertWarning(string message, bool isPostBackReference = true, string js = "")
            {
                FineUICore.Alert aa = new FineUICore.Alert();
                aa.AlertWarning(message, isPostBackReference, js);
            }

            #endregion

            #region Notify 消息框

            /// <summary>
            /// 信息消息框
            /// </summary>
            /// <param name="message">警告信息</param>
            /// <param name="js">然后执行的js代码</param>
            public void NotifyInformation(string message, string js = null)
            {
                NotifyBase(message, js, MessageBoxIcon.Information);
            }

            /// <summary>
            /// 问题消息框
            /// </summary>
            /// <param name="message">警告信息</param>
            /// <param name="js">然后执行的js代码</param>
            public void NotifyQuestion(string message, string js = null)
            {
                NotifyBase(message, js, MessageBoxIcon.Question);
            }

            /// <summary>
            /// 警告消息框
            /// </summary>
            /// <param name="message">警告信息</param>
            /// <param name="js">然后执行的js代码</param>
            public void NotifyWarning(string message, string js = null)
            {
                NotifyBase(message, js, MessageBoxIcon.Warning);
            }

            /// <summary>
            /// 错误消息框
            /// </summary>
            /// <param name="message">错误信息</param>
            /// <param name="js">然后执行的js代码</param>
            public void NotifyError(string message, string js = null)
            {
                NotifyBase(message, js, MessageBoxIcon.Error);
            }

            /// <summary>
            /// 成功消息框
            /// </summary>
            /// <param name="message">错误信息</param>
            /// <param name="js">然后执行的js代码</param>
            public void NotifySuccess(string message, string js = null)
            {
                NotifyBase(message, js, MessageBoxIcon.Success);
            }

            private void NotifyBase(string message, string js = null, FineUICore.MessageBoxIcon icon = MessageBoxIcon.Information)
            {
                Notify nf = new Notify();
                nf.GetShowNotify();
                nf.Message = message;
                nf.MessageBoxIcon = icon;
                if (!string.IsNullOrEmpty(js)) nf.HideScript = js;
                nf.Show();
            }

            #endregion

            
        }
    }
}