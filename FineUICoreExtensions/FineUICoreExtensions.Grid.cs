﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeSql;
using Microsoft.AspNetCore.Http;


namespace FineUICore
{
    public static partial class FineUICoreExtensions
    {
        /// <summary>
        /// 绑定BindGrid
        /// </summary>
        /// <param name="grid">grid控件帮助类</param>
        /// <param name="fc">表单</param>
        /// <param name="pageSizeID">分页size控件id</param>
        /// <param name="select">查询表达式</param>
        /// <returns></returns>
        public static List<T> BindGrid<T>(this GridAjaxHelper grid, IFormCollection fc, string pageSizeID, ISelect<T> select) where T : class
        {
            int pageSize = int.Parse(fc[pageSizeID]);
            
            /*
            var list = fsql.Select<Topic>()
                        .Where(a => a.Id > 10)
                        .Count(out var total) //总记录数量
                        .Page(1, 20)
                        .Tolist();
             */

            var tempData = select.Count(out var recordCount).Page(int.Parse(fc[grid.Source.ID+"_pageIndex"]) + 1, pageSize).ToList();

            BindGrid(grid,  tempData, fc, pageSize, (int)recordCount);

            return tempData;
        }

        /// <summary>
        /// 绑定BindGrid
        /// </summary>
        /// <param name="grid">grid控件帮助类</param>
        /// <param name="fc">表单</param>
        /// <param name="dataSource">数据源</param>
        /// <param name="pageSize">设置每页显示项数</param>
        /// <param name="recordCount">数据总数</param>
        /// <returns></returns>
        public static void BindGrid(this GridAjaxHelper grid, dynamic dataSource, IFormCollection fc, int pageSize, int recordCount)
        {
            grid.DataSource(dataSource, fc[grid.Source.ID + "_fields[]"].ToString().Split(','));
            grid.RecordCount(recordCount);
            grid.PageSize(pageSize);
        }
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderName">渲染方法名称</param>
        /// <param name="enumType">枚举</param>
        /// <returns></returns>
        public static string GetRendererFunctionByEnum(string renderName,Type enumType)
        {
            /*
        function renderGender(value, params) {
            switch (value) {
                case 1: return '';
                case 1: return '';
                case 1: return '';                    
            }
            return '';
        }
             */

            StringBuilder sb =new StringBuilder();
            sb.AppendLine("        function "+ renderName + "(value, params) {");
            sb.AppendLine("            switch (value) {");
            foreach (var item in Enum.GetValues(enumType))
            {
                sb.AppendLine(string.Format("                case {0}: return '{1}';", (int)item, item));
            }
            sb.AppendLine("            }");
            sb.AppendLine("            return '';");
            sb.AppendLine("        }");
            return sb.ToString();
        }


    }
}
