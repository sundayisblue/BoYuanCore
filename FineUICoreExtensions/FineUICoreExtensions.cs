﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FineUICore
{
    public partial class FineUICoreExtensions

    {
        /// <summary>
        /// 显示信息并跳转到指定页面
        /// </summary>
        /// <param name="alert"></param>
        /// <param name="message"></param>
        /// <param name="url"></param>
        /// <param name="js"></param>
        /// <param name="title"></param>
        public static void AlertInforAndRedirect(this FineUICore.Alert alert, string message, string url, string js = "", string title = "")
        {
            FineUICore.Alert.ShowInParent(message, title, MessageBoxIcon.Information, ActiveWindow.GetHidePostBackReference() + string.Format("self.location='{0}';{1} ", url, js));
        }

        /// <summary>
        /// 提示信息(弹层用)
        /// </summary>
        /// <param name="alert"></param>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        /// <param name="js">显示消息后 执行的js</param>
        /// <param name="title">标题</param>
        public static void AlertInfor(this FineUICore.Alert alert, string message, bool isPostBackReference = true, string js = "", string title = "")
        {
            alert.AlertBase(message, isPostBackReference, js, MessageBoxIcon.Information, title);
            //if (isPostBackReference)
            //{
            //    //弹层页用,结合window的Onclose事件使用
            //    FineUICore.Alert.ShowInParent(message, "提示对话框", MessageBoxIcon.Information, ActiveWindow.GetHidePostBackReference());

            //    //弹层页用
            //    //Alert.ShowInParent(message, "提示对话框", MessageBoxIcon.Information, ActiveWindow.GetHideRefreshReference());

            //    //单页 并刷新当前页
            //    //Alert.ShowInParent(message, "提示对话框", MessageBoxIcon.Information, "window.location.reload()");
            //}
            //else
            //    FineUICore.Alert.ShowInParent(message, MessageBoxIcon.Information);

        }

        /// <summary>
        /// 提示信息（单页用 并刷新当前页）
        /// </summary>
        /// <param name="alert"></param>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        /// <param name="js">显示消息后 执行的js</param>
        /// <param name="title">标题</param>
        public static void AlertInforBySingerPage(this FineUICore.Alert alert, string message, bool isPostBackReference = true, string js = "", string title = "")
        {
            if (isPostBackReference)
                alert.AlertBase(message, isPostBackReference, "window.location.reload()", MessageBoxIcon.Information, title); //单页 并刷新当前页
            else
                alert.AlertBase(message, isPostBackReference, string.Empty, MessageBoxIcon.Information, title); //单页 并刷新当前页
        }

        /// <summary>
        /// 报错信息
        /// </summary>
        /// <param name="alert"></param>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        /// <param name="js">显示消息后 执行的js</param>
        /// <param name="title">标题</param>
        public static void AlertError(this FineUICore.Alert alert, string message, bool isPostBackReference = true, string js = "", string title = "")
        {
            alert.AlertBase(message, isPostBackReference, js, MessageBoxIcon.Error, title);
        }

        /// <summary>
        /// 成功信息
        /// </summary>
        /// <param name="alert"></param>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        /// <param name="js">显示消息后 执行的js</param>
        /// <param name="title">标题</param>
        public static void AlertSuccess(this FineUICore.Alert alert, string message, bool isPostBackReference = true, string js = "", string title = "")
        {
            alert.AlertBase(message, isPostBackReference, js, MessageBoxIcon.Success, title);
        }

        /// <summary>
        /// 疑问信息
        /// </summary>
        /// <param name="alert"></param>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        /// <param name="js">显示消息后 执行的js</param>
        /// <param name="title">标题</param>
        public static void AlertQuestion(this FineUICore.Alert alert, string message, bool isPostBackReference = true, string js = "", string title = "")
        {
            alert.AlertBase(message, isPostBackReference, js, MessageBoxIcon.Question, title);
        }

        /// <summary>
        /// 警告信息
        /// </summary>
        /// <param name="alert"></param>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        /// <param name="js">显示消息后 执行的js</param>
        /// <param name="title">标题</param>
        public static void AlertWarning(this FineUICore.Alert alert, string message, bool isPostBackReference = true, string js = "", string title = "")
        {
            alert.AlertBase(message, isPostBackReference, js, MessageBoxIcon.Warning, title);
        }

        private static void AlertBase(this FineUICore.Alert alert, string message, bool isPostBackReference = true, string js = "", MessageBoxIcon icon = MessageBoxIcon.Information, string title = "")
        {
            if (isPostBackReference)
                FineUICore.Alert.ShowInParent(message, title, icon, ActiveWindow.GetHidePostBackReference() + js);
            else
            {
                if (string.IsNullOrEmpty(js))
                    FineUICore.Alert.ShowInParent(message, title, icon);
                else
                    FineUICore.Alert.ShowInParent(message, title, icon, js);
            }
        }
    }
}
