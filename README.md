> 说明文档备份地址: https://www.jianshu.com/p/a218d710bde6

欢迎加入Q群:275110998

### 0 介绍BoYuanCore框架
**项目介绍**
实现页面权限基于权限角色的后台框架，配套完善代码生成工具，可开箱即用，适用于中小型项目快速开发。
项目为.Net Core 3.1或.Net 5，使用简单的service分层架构，前端为FineUICore , 数据层使用FreeSql ORM+雪花算法实体模式，兼容各种不同的数据库迁移(支持的数据库：SqlServer, Mysql, Postgresql, Oracle 等)，并有完善的异常拦截写入日志功能。
优秀的编码体验，层次分明，简单易学，从而实现快速开发的目的，或入门学习.Net Core。
建议使用vs2019以上版本开发工具 ，数据库随意。

###### 平时在工作或学习中，受到的FineUI和FreeSql帮助比较大，特此开源自己的项目底层架构，分享开源精神。如果对您有所有帮助，给项目点上星星，支持下开源作者。

**FineUI 特点**
企业级 ASP.NET UI 控件库,基于jquery实现。封装了很多常用的功能，少写很多js和css便于快速开发后台或软件管理等项目，是一个很不错的工具框架。
> 演示地址 https://core.fineui.com 
 
**FreeSql 特点**
可能是史上最全面和稳定的ORM框架，个人认为没有之一。社区活跃，有bug不过夜，各种骚套路玩法可满足任何人不同的口味。强烈推荐大家了解下FreeSql
> 官网地址 https://github.com/dotnetcore/FreeSql (推荐大家看看wiki和issues)

### 1 下载FineUI相关框架组件。
由于版权限制，本框架需要到知识星球载FineUIPro.dll 和 FineUICore.dll这2个文件。**免费社区** ：[https://wx.zsxq.com/dweb2/index/group/28888555288121](https://wx.zsxq.com/dweb2/index/group/28888555288121)

![知识星球首页置顶可以下载相关dll](https://images.gitee.com/uploads/images/2020/0925/142001_0d5af47c_436641.jpeg "1.jpg")
知识星球首页置顶可以下载相关dll


### 2 下载BoYuanCore项目，并配置
如果有问题或建议欢迎到gitee上留言。gitee有联系方式，BoYuanCore项目地址如下：
> https://gitee.com/sundayisblue/BoYuanCore

下载项目后，需要重新引用FineUIPro.dll 和 FineUICore.dll。

![删除FineUICore引用，并重新引用FineUICore.dll](https://images.gitee.com/uploads/images/2020/0925/142215_540320ac_436641.jpeg "1.jpg")
[删除FineUICore引用，并重新引用FineUICore.dll]

![删除FineUICore引用，并重新引用FineUICore.dll](https://images.gitee.com/uploads/images/2020/0925/142303_778fd443_436641.jpeg "1.jpg")
[删除FineUICore引用，并重新引用FineUICore.dll]

![删除FineUIPro引用，并重新引用FineUIPro.dll](https://images.gitee.com/uploads/images/2020/0925/142436_e235938f_436641.jpeg "1.jpg")
[删除FineUIPro引用，并重新引用FineUIPro.dll]

![右键项目浏览添加引用FineUIPro.dll或引用FineUICore.dll](https://images.gitee.com/uploads/images/2020/0925/142557_59f690c1_436641.jpeg "1.jpg")
[右键项目浏览添加引用FineUIPro.dll或引用FineUICore.dll]

加载完成后，重新生成解决方案。初次生成可能有些慢，需要从nuget下载相关组件dll。

接下来配置资源文件res文件相关。在项目doc文件夹里，解压res.7z。
![解压res.7z资源文件](https://images.gitee.com/uploads/images/2020/0925/142742_5c304b16_436641.jpeg "1.jpg")
[解压res.7z资源文件]

复制res到项目 BoYuanCore.Web\wwwroot 和 BoYuanCore.CodeGenerator 文件夹里

![BoYuanCore.Web\wwwroot](https://images.gitee.com/uploads/images/2020/0925/142857_959bf0e5_436641.jpeg "1.jpg")
[BoYuanCore.Web\wwwroot]

![BoYuanCore.CodeGenerator](https://images.gitee.com/uploads/images/2020/0925/142959_dc1119a5_436641.jpeg "1.jpg")
[BoYuanCore.CodeGenerator]

至此项目配置完成。

**注意 res文件夹不要包含在项目里(由于res文件太多，编译会很慢)，发布项目时候手动复制到服务器环境里。**

### 3 创建项目数据库
重新生成解决方案后没有异常后，先运行代码生成工具，如下图
![先启动BoYuanCore.CodeGenerator项目](https://images.gitee.com/uploads/images/2020/0925/143110_4f561c79_436641.jpeg "1.jpg")
[先启动BoYuanCore.CodeGenerator项目]

代码生成工具是fineuipro，webform架构，这种webform开发快，不影响主业务项目。

创建数据库，如下图选择数据库，填写连接字符串，点击即可完成创建。
![创建数据库基础表结构](https://images.gitee.com/uploads/images/2020/0925/143213_29004f8d_436641.jpeg "1.jpg")
[创建数据库基础表结构]

### 4 使用代码生成工具

作为示例创建一个新的表，以Sql server数据库为例创建news表。主键id设置为bigint(对应C#是long类型，雪花主键必须以long为主键，用雪花主键方便数据库做迁移)，非自增。
![创建news表结构](https://images.gitee.com/uploads/images/2020/0925/143422_d28f3d53_436641.jpeg "1.jpg")
[创建news表结构]

在代码生成工具中，先连接数据库，选择要生成实体的表news，最后点击**生成代码**。不建议直接把代码直接生成到项目里，怕不小心覆盖，默认在C盘code文件夹下，手动加入到项目里比较好。
![生成对应数据库的实体模型](https://images.gitee.com/uploads/images/2020/0925/143910_1f5ae3d5_436641.jpeg "1.jpg")
[生成对应数据库的实体模型]

![手动加入entity模型](https://images.gitee.com/uploads/images/2020/0925/144702_ddef27b0_436641.jpeg "1.jpg")
[手动加入entity模型]



同样选择数据库相关表，点击**生成简单代码**，在C盘生成相关页面代码。
![生成页面代码，也可以预览单页代码](https://images.gitee.com/uploads/images/2020/0925/144743_56ae4f90_436641.jpeg "1.jpg")
[生成页面代码，也可以预览单页代码]

![把生成的页面代码复制到项目路径下](https://images.gitee.com/uploads/images/2020/0925/144604_28b45fc4_436641.jpeg "1.jpg")
[把生成的页面代码复制到项目路径下]

![创建后的页面代码](https://images.gitee.com/uploads/images/2020/0925/144904_8515d39d_436641.jpeg "1.jpg")
[创建后的页面代码]


至此所有代码生成完成。

### 5 运行Core项目

这里我们选择core的启动项目方式(非iis方式，推荐core启动方式)
![选择core的启动](https://images.gitee.com/uploads/images/2020/0925/145309_e2d1667c_436641.jpeg "1.jpg")
[选择core的启动]

启动后，会有登录页面 用户名admin 默认密码是123456。core启动方式会有控制台显示，方便查看运行时的日志，包括运行的sql等。
![登录页面显示](https://images.gitee.com/uploads/images/2020/0925/145220_49f168fd_436641.jpeg "1.jpg")
[登录页面显示]

![首页显示](https://images.gitee.com/uploads/images/2020/0925/145355_31439cf3_436641.jpeg "1.jpg")
[首页显示]

添加页面组件。右键顶点，是添加根目录。右键文件夹可以添加子页面。
![添加页面组件](https://images.gitee.com/uploads/images/2020/0925/145430_8244b6d7_436641.jpeg "1.jpg")
[添加页面组件]

![添加子页面](https://images.gitee.com/uploads/images/2020/0925/153845_2334d522_436641.png "屏幕截图.png")
[添加子页面]

创建权限角色
![创建权限角色-新闻编辑角色](https://images.gitee.com/uploads/images/2020/0925/145628_d82b4340_436641.jpeg "1.jpg")
[创建权限角色-新闻编辑角色]

添加用户，并给于**新闻编辑**权限角色
![添加用户](https://images.gitee.com/uploads/images/2020/0925/145815_b358a6a7_436641.png "屏幕截图.png")
[添加用户]

用新用户登录后界面显示

![用新用户登录后界面显示](https://images.gitee.com/uploads/images/2020/0925/145852_0e5fab47_436641.png "屏幕截图.png")


自己试试CURD功能吧。

### 6 更改数据库类型
代码生成工具的数据库类型不需要更改，有下拉框选项自己设置即可。

如果是项目需要，更改数据库类型，比如说sqlserver 改成 oracle  mysql pgsql 等(如下图)，并在appsetting.json更改对应数据类的连接字符串即可。![输入图片说明](https://images.gitee.com/uploads/images/2021/0202/142516_21285950_436641.png "db.png")

### 7 如果页面提示错误码: xxxxxxxxxxxxxxxx ,请在项目根目录下logs文件里，找对应的错误log，即可知道bug。


#### 如果觉得对您有所帮助，欢迎您点亮star或您支持下作者

<img src="https://gitee.com/uploads/images/2019/0423/190419_15b3388e_436641.png" width="200" >

<img src="https://gitee.com/uploads/images/2019/0423/190842_1c6bdb00_436641.png" width="200" >